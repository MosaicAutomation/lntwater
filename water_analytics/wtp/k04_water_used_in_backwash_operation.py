import sys
from datetime import datetime, timedelta
import decimal
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def wtp_calculate_water_used_in_backwash_tank(plant_type, asset_type, source_asset_type):
    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")
    yesterday_date = str_yesterday + " 00:00:00"

    resource = AWSService(Services.DYNAMO_DB)
    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    wtp_plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(Region.R1),
        FilterExpression=Attr('document.plantId').contains(plant_type) & Attr('document.attributes.assetType').contains(asset_type)
    )
    for plant in wtp_plants['Items']:
        parameters = plant[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        k01_wtp_tag = parameters[Constants.WTP_K01_WATER_PROCESSED_PER_DAY][Constants.TAG_ID_KEY]
        try:
            asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
            tanks = asset_table.query(
                KeyConditionExpression=Key('id').eq(plant[Constants.PLANT_ID_KEY]),
                FilterExpression=Attr('document.attributes.tankModel').eq(source_asset_type)
            )
            for tank in tanks['Items']:
                tank_parameters = tank[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
                fq01_tag = tank_parameters[Constants.WTP_BACKWASH_TANK_WATER_FLOW_TOTALIZER][Constants.TAG_ID_KEY]

                k04_tag = tank_parameters[Constants.WTP_VOLUME_USED_IN_BACKWASH_OPERATION][Constants.TAG_ID_KEY]
                k01_data = water_utilities.get_calculated_opera_water_data(resource, decimal.Decimal(k01_wtp_tag), str_yesterday)
                k01_wtp_value = decimal.Decimal(k01_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
                fq01_data = water_utilities.get_calculated_opera_water_data(resource, decimal.Decimal(fq01_tag), str_yesterday)
                fq01_wtp_value = decimal.Decimal(fq01_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
                final_value = (fq01_wtp_value / decimal.Decimal(k01_wtp_value)) * 100
                final_object = water_utilities.create_calculated_object(k04_tag, round(final_value, 2), yesterday_date)
                water_utilities.save_calculated_water_tag(resource, final_object)
                logger.info("WTP - BACKWASH OPERATION - Calculated Object Saved %s", final_object)
        except WaterAnalyticsException as err:
            logger.error("WTP - BACKWASH OPERATION - WTP %s", err)
            print(err)
        except Exception as err:
            print(err)
            logger.error("WTP - BACKWASH OPERATION - WTP %s", err)


if __name__ == '__main__':
    wtp_calculate_water_used_in_backwash_tank('WTP', 'Treatment Plant', 'Backwash Tank')