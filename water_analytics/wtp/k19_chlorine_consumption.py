import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def calculate_wtp_k19(plant_type, asset_type):
    """
    This function calculate the Unit consumption of Alum for supplying MLD of water.

    :param plant_type:
    :param asset_type:
    :return:
    """
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    yesterday_date = str_yesterday + " 00:00:00"

    resource = AWSService(Services.DYNAMO_DB)
    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(Region.R1),
        FilterExpression=Attr('document.plantId').contains(plant_type) & Attr('document.attributes.assetType').contains(
            asset_type)
    )
    for plant in plants['Items']:
        try:
            parameters = plant[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
            k01 = parameters[Constants.WTP_K01_WATER_PROCESSED_PER_DAY][Constants.TAG_ID_KEY]
            chlorine_tag_key = parameters[Constants.WTP_CHLORINE_CONSUMPTION][Constants.TAG_ID_KEY]
            k01_data = water_utilities.get_calculated_opera_water_data(resource, decimal.Decimal(k01), str_yesterday)
            k01_value = decimal.Decimal(k01_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])

            try:
                k19 = parameters[Constants.WTP_K19_UNIT_CHLORINE_CONSUMPTION][Constants.TAG_ID_KEY]
                alum_data = water_utilities.get_raw_opera_water_data(
                    resource, decimal.Decimal(chlorine_tag_key), str_yesterday)
                chlorine_value = decimal.Decimal(alum_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
                logger.info('WTP - K19 - Value of %s is %s and Value of %s is %s',
                            k01, k01_value, chlorine_tag_key, chlorine_value)
                k19_value = chlorine_value / k01_value
                final_obj = water_utilities.create_calculated_object(k19, round(k19_value, 3), yesterday_date)
                print("Final Object : ", final_obj)
                water_utilities.save_calculated_water_tag(resource, final_obj)
            except WaterAnalyticsException as err:
                logger.error('K19 - Error Occurred in K19 Calculations : %s', err)
        except WaterAnalyticsException as err:
            logger.error(err)
        except Exception as err:
            logger.error(err)


if __name__ == '__main__':
    calculate_wtp_k19('WTP', 'Treatment Plant')
