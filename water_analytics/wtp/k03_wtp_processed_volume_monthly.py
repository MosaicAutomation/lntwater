import sys
# sys.path.append("D:\MB\Final Version\lnt-water-analytics\\")
from datetime import datetime
import logging
from boto3.dynamodb.conditions import Key, Attr
from dateutil.relativedelta import relativedelta
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *


logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_monthly_processed_water_volume(resource, tag_id):
    last_month_day = datetime.today() - relativedelta(months=1)
    last_month_yyyy_mm = last_month_day.strftime('%Y-%m')
    water_tag_table = resource.get_table(Tables.OPERA_WATER_CALCULATED_TAGS)
    raw_data = water_tag_table.query(
        KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(tag_id) & Key(Constants.TIME_STAMP).begins_with(
            last_month_yyyy_mm),
    )
    monthly_processed_volume = 0
    for raw in raw_data['Items']:
        monthly_processed_volume += decimal.Decimal(raw[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
    return monthly_processed_volume


def calculate_wtp_monthly_processed_volume(plant_type):
    today = datetime.today()
    first_day_of_month = date(today.year, today.month, 1)
    first_day_of_month_timestamp = first_day_of_month.strftime('%Y-%m-%d') + " 00:00:00"

    resource = AWSService(Services.DYNAMO_DB)

    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    wtp_plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(Region.R1),
        FilterExpression=Attr('document.plantId').contains(plant_type)
    )
    for plant in wtp_plants['Items']:
        parameters = plant[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        processed_per_day = parameters[Constants.WTP_WATER_PROCESSED_PER_DAY][Constants.TAG_ID_KEY]
        processed_per_month = parameters[Constants.WTP_WATER_PROCESSED_PER_MONTH][Constants.TAG_ID_KEY]
        logger.info("WTP - MONTHLY PROCESSED VOLUME - processed_per_day ID : %s | processed_per_month ID %s", processed_per_day, processed_per_month)
        monthly_volume = get_monthly_processed_water_volume(resource, decimal.Decimal(processed_per_day))
        logger.info("WTP - MONTHLY PROCESSED VOLUME - Monthly Volume : %s", monthly_volume)
        calculated_obj = water_utilities.create_calculated_object(
            processed_per_month, round(monthly_volume, 2), first_day_of_month_timestamp)
        logger.info('WTP - MONTHLY PROCESSED VOLUME - calculated obj : %s', calculated_obj)
        water_utilities.save_calculated_water_tag(resource, calculated_obj)


if __name__ == '__main__':
    calculate_wtp_monthly_processed_volume('WTP')
