import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_raw_value_of_filter_tag(resource, tag_id, day):
    try:
        water_tag_table = resource.get_table(Tables.OPERA_WATER_TAGS)
        raw_data = water_tag_table.query(
            KeyConditionExpression=Key('tagId').eq(tag_id) & Key('timestamp').begins_with(day),
            ScanIndexForward=False,
            Limit=1
        )
        raw_water_data = raw_data['Items'][0]
        return raw_water_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY], raw_water_data[Constants.TIME_STAMP]
    except IndexError as err:
        logger.error('FILTER STATUS - No Data found for tag id %s and for date %s - error as %s ', str(tag_id), day, err)
        return '0', '0'


def check_filter_in_operation(resource, filter_name, filter_parameters, day):
    """
    This function checks whether filter is in the Operation mode or not.
    The filter will be in Operation mode
        if filterBedInletValveStatus and filteredWaterOutletValveStatus are OPEN. i.e. value 1
    :param resource:
    :param filter_name:
    :param filter_parameters:
    :param day:
    :return:
    """

    in_let_tag = filter_parameters[Constants.FILTER_BED_INLET_VALVE_STATUS][Constants.TAG_ID_KEY]
    out_let_tag = filter_parameters[Constants.FILTERED_WATER_OUTLET_VALVE_STATUS][Constants.TAG_ID_KEY]
    fio_tag = filter_parameters[Constants.FILTER_IN_OPERATION][Constants.TAG_ID_KEY]
    in_let_val, in_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(in_let_tag), day)
    out_let_val, out_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(out_let_tag), day)
    if in_let_val == out_let_val == '1':
        print("Filter Is in Operation : ", filter_name)
        logger.info('FILTER STATUS - Filter %s is in Operation ', filter_name)
        obj = water_utilities.create_calculated_object(fio_tag, 1, in_time_stamp)
        print('Calculated Object : ', obj)
        water_utilities.save_calculated_water_tag(resource, obj)
        return True
    else:
        logger.info('FILTER STATUS - Filter %s is NOT in Operation ', filter_name)
        return False


def check_filter_in_maintenance(resource, filter_name, filter_parameters, day):
    """
    This function checks whether filter is in the Maintenance mode or not.
    The filter will be in Maintenance mode
        if all(
        filterBedInletValveStatus, filteredWaterOutletValveStatus,
        backwashInletValveStatus, airScouringValveStatus,
        outletValveToDirtyBackwashWaterChannelStatus)
        valves are closed. i.e value 2

    :param resource:
    :param filter_name:
    :param filter_parameters:
    :param day:
    :return boolean:
    """
    in_let_tag = filter_parameters[Constants.FILTER_BED_INLET_VALVE_STATUS][Constants.TAG_ID_KEY]
    out_let_tag = filter_parameters[Constants.FILTERED_WATER_OUTLET_VALVE_STATUS][Constants.TAG_ID_KEY]
    backwash_in = filter_parameters[Constants.BACKWASH_INLET_VALVE_STATUS][Constants.TAG_ID_KEY]
    air_source_in = filter_parameters[Constants.AIR_SOURING_VALVE_STATUS][Constants.TAG_ID_KEY]
    backwash_out = filter_parameters[Constants.BACKWASH_OUTLET_VALVE_STATUS][Constants.TAG_ID_KEY]

    fim_tag = filter_parameters[Constants.FILTER_IN_MAINTENANCE][Constants.TAG_ID_KEY]

    # Getting Values of each tag
    in_let_val, in_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(in_let_tag), day)
    out_let_val, out_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(out_let_tag), day)
    backwash_in_val, in_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(backwash_in), day)
    air_source_in_val, out_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(air_source_in), day)
    backwash_out_val, in_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(backwash_out), day)

    if backwash_out_val == in_let_val == out_let_val == backwash_in_val == air_source_in_val == '2':
        print("Filter Is in Maintenance : ", filter_name)
        logger.info('FILTER STATUS - Filter %s is in Maintenance ', filter_name)
        obj = water_utilities.create_calculated_object(fim_tag, 1, in_time_stamp)
        water_utilities.save_calculated_water_tag(resource, obj)
        return True
    else:
        logger.info('FILTER STATUS - Filter %s is not in Maintenance ', filter_name)
        return False


def check_filter_in_backwash(resource, filter_name, filter_parameters, day):
    """
    This function checks whether filter is in the Backwash mode or not.
    The filter will we in Backwash mode
        if backwashInletValveStatus and airScouringValveStatus is OPEN i.e. value 1
    :param resource:
    :param filter_name:
    :param filter_parameters:
    :param day:
    :return:
    """
    backwash_in = filter_parameters[Constants.BACKWASH_INLET_VALVE_STATUS][Constants.TAG_ID_KEY]
    air_source_in = filter_parameters[Constants.AIR_SOURING_VALVE_STATUS][Constants.TAG_ID_KEY]
    fib_tag = filter_parameters[Constants.FILTER_IN_BACKWASH][Constants.TAG_ID_KEY]
    backwash_in_val, in_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(backwash_in), day)
    air_source_in_val, out_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(air_source_in), day)
    if backwash_in_val == air_source_in_val == '1':
        logger.info('FILTER STATUS - Filter %s is in Backwash ', filter_name)
        obj = water_utilities.create_calculated_object(fib_tag, 1, in_time_stamp)
        print('Calculated Object : ', obj)
        water_utilities.save_calculated_water_tag(resource, obj)
        return True
    else:
        logger.info('FILTER STATUS - Filter %s is NOT in Backwash ', filter_name)
        return False


def check_filter_in_dirty_backwash(resource, filter_name, filter_parameters, day):
    """
    This function checks whether filter is in the Dirty Backwash mode or not.
    The Filter will be in Dirty Backwash mode
        if outletValveToDirtyBackwashWaterChannelStatus is OPEN i.e. value 1
    :param resource:
    :param filter_name:
    :param filter_parameters:
    :param day:
    :return boolean:
    """
    backwash_out = filter_parameters[Constants.BACKWASH_OUTLET_VALVE_STATUS][Constants.TAG_ID_KEY]
    fidb_tag = filter_parameters[Constants.FILTER_IN_DIRTY_BACKWASH][Constants.TAG_ID_KEY]
    backwash_out_val, in_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(backwash_out), day)
    if backwash_out_val == '1':
        logger.info('FILTER STATUS - Filter %s is in Dirty Backwash ', filter_name)
        obj = water_utilities.create_calculated_object(fidb_tag, 1, in_time_stamp)
        print('Calculated Object : ', obj)
        water_utilities.save_calculated_water_tag(resource, obj)
        return True
    else:
        logger.info('FILTER STATUS - Filter %s is NOT in Dirty Backwash ', filter_name)
        return False


def get_filter_status(resource, filter_name, filter_parameters, day):
    # Raw Tags
    print('Parameters : ', filter_parameters)
    in_let_tag = filter_parameters[Constants.FILTER_BED_INLET_VALVE_STATUS][Constants.TAG_ID_KEY]
    print(in_let_tag)
    out_let_tag = filter_parameters[Constants.FILTERED_WATER_OUTLET_VALVE_STATUS][Constants.TAG_ID_KEY]
    print(out_let_tag)
    backwash_in = filter_parameters[Constants.BACKWASH_INLET_VALVE_STATUS][Constants.TAG_ID_KEY]
    air_source_in = filter_parameters[Constants.AIR_SOURING_VALVE_STATUS][Constants.TAG_ID_KEY]
    backwash_out = filter_parameters[Constants.BACKWASH_OUTLET_VALVE_STATUS][Constants.TAG_ID_KEY]

    # calculated Tags
    fio_tag = filter_parameters[Constants.FILTER_IN_OPERATION][Constants.TAG_ID_KEY]
    fib_tag = filter_parameters[Constants.FILTER_IN_BACKWASH][Constants.TAG_ID_KEY]
    fim_tag = filter_parameters[Constants.FILTER_IN_MAINTENANCE][Constants.TAG_ID_KEY]
    fidb_tag = filter_parameters[Constants.FILTER_IN_DIRTY_BACKWASH][Constants.TAG_ID_KEY]

    # Getting Values of each tag
    in_let_val, in_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(in_let_tag), day)
    out_let_val, out_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(out_let_tag), day)
    backwash_in_val, in_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(backwash_in), day)
    air_source_in_val, out_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(air_source_in), day)
    backwash_out_val, in_time_stamp = get_raw_value_of_filter_tag(resource, decimal.Decimal(backwash_out), day)

    obj = 0
    if backwash_out_val == '1':
        print('Filter in Dirty Backwash')
        logger.info("FILTER STATUS - Filter %s is in Dirty Backwash ", filter_name)
        obj = water_utilities.create_calculated_object(fidb_tag, 1, in_time_stamp)
    elif backwash_out_val == in_let_val == out_let_val == backwash_in_val == air_source_in_val == '2':
        print('Filter in Maintenance')
        logger.info("FILTER STATUS - Filter %s is in Maintenance ", filter_name)
        obj = water_utilities.create_calculated_object(fim_tag, 1, in_time_stamp)
    elif in_let_val == out_let_val == '1':
        print('Filter in Operation')
        logger.info("FILTER STATUS - Filter %s is in Operation", filter_name)
        obj = water_utilities.create_calculated_object(fio_tag, 1, in_time_stamp)
    elif backwash_in_val == air_source_in_val == '1':
        print('Filter in Backwash')
        logger.info("FILTER STATUS - Filter %s is in Backwash", filter_name)
        obj = water_utilities.create_calculated_object(fib_tag, 1, in_time_stamp)
    print('Calculated Object : ', obj)


def wtp_check_filter_mode(plant_type, asset_type, source_asset_type):
    """
    This function is used to check WTP Filter modes.
    The Modes are:
        1. in Operation
        2. in Maintenance
        3. in Backwash
        4. in Dirty Backwash

    :param plant_type:
    :param asset_type:
    :param source_asset_type:
    :return:
    """
    today_date = datetime.now().strftime("%Y-%m-%d")

    # today_date = '2018-01-16'

    # str_yesterday = yesterday_date.strftime("%Y-%m-%d")
    resource = AWSService(Services.DYNAMO_DB)
    # Getting Plant details and retrieve plant ids based on the region id
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, plant_type, asset_type)
    logger.info("FILTER STATUS - Plant ID's : %s ", plant_ids)

    # Search for a Source, For that WaterAsset table is used.
    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    for key in plant_ids:
        print('ID : ', key)
        logger.info("FILTER STATUS - Getting data for Plant ID : %s", key)
        try:
            filters = asset_table.query(
                KeyConditionExpression=Key('id').eq(key),
                FilterExpression=Attr('document.attributes.assetType').eq(source_asset_type)
            )
            for wtp_filter in filters['Items']:
                filter_name = wtp_filter[Constants.ASSET_ID_KEY]
                parameters = wtp_filter[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
                try:
                    if check_filter_in_operation(resource, filter_name, parameters, today_date):
                        logger.info("FILTER STATUS - Filter %s is in Operation", filter_name)
                    elif check_filter_in_dirty_backwash(resource, filter_name, parameters, today_date):
                        logger.info("FILTER STATUS - Filter %s is in Dirty Backwash ", filter_name)
                    elif check_filter_in_backwash(resource, filter_name, parameters, today_date):
                        logger.info("FILTER STATUS - Filter %s is in Backwash", filter_name)
                    elif check_filter_in_maintenance(resource, filter_name, parameters, today_date):
                        logger.info("FILTER STATUS - Filter %s is in Maintenance ", filter_name)
                except WaterAnalyticsException as err:
                    print(err)
                    logger.error("FILTER STATUS - %s", err)
        except WaterAnalyticsException as err:
            print("FILTER STATUS - %s", err)
        except Exception as err:
            print("FILTER STATUS - %s", err)


if __name__ == '__main__':
    wtp_check_filter_mode('WTP', 'Treatment Plant', 'Filter')
