import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.RWPH_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)
wa = "WATER AVAILABILITY"


def get_opera_water_volume(resource, tag_value, asset_id):
    """
    This function checks for the lookup value of the water level from OperaWaterLookup Table
    :param resource:
    :param tag_value:
    :param asset_id:
    :return: volume; Volume of the water
    """
    # Have a lookup table OperaWaterLookupTable based on the asset id and level
    # returns a Volume of the Water based on the level
    opera_water_lookup_table = resource.get_table(Tables.OPERA_WATER_LOOKUP_TABLE)
    try:
        lookup_data = opera_water_lookup_table.get_item(
            Key={'level': tag_value, 'assetId': asset_id}
        )
        lookup_item = lookup_data['Item']
        return lookup_item[Constants.DOCUMENT_KEY][Constants.VOLUME_KEY]
    except Exception as err:
        print('Error ', err)
        logger.error("%s - No lookup found for asset id %s and Level %s ", wa, asset_id, tag_value)
        logger.error(err)
        raise WaterAnalyticsException(wa + " - No lookup found for asset id %s and Level " + str(asset_id) + str(tag_value))


def get_raw_water_data_and_calculate_tag(resource, water_intake_tagid, asset_id, water_volume_obj):
    """
    This function get raw water data from OperaWaterTags table and does calculation for OperaWaterTag Water availability
    :param resource:
    :param water_intake_tagid:
    :param asset_id:
    :param water_volume_obj:
    :return: calculated_tag_obj; the calculated tag to OpetaWaterCalculatedTags table
    """
    today_date = datetime.now().strftime("%Y-%m-%d")

    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    today_date = today_date + " 00:00:00"
    # Getting Raw Data from OperaWaterTag table
    water_tag_table = resource.get_table(Tables.OPERA_WATER_TAGS)
    # Getting Recent Data based on yesterday date
    try:
        raw_data = water_tag_table.query(
            KeyConditionExpression=Key('tagId').eq(water_intake_tagid) & Key('timestamp').begins_with(str_yesterday),
            ScanIndexForward=False,
            Limit=1
        )
        print('RAW DATA : ', raw_data)
        water_data = raw_data['Items'][0]
        logger.info('%s - Raw Water Data Found for Water Intake id %s and date %s - %s', wa, water_intake_tagid, str_yesterday, water_data)
        tag_value = water_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
        print('Tag Value : ', tag_value)

        volume = get_opera_water_volume(resource, tag_value, asset_id)
        print("Volume : ", volume)
        tag_yesterday = str_yesterday + " 00:00:00"

        calculated_document = dict()
        calculated_document[Constants.TAG_VALUE_KEY] = str(volume)
        calculated_document[Constants.TAG_ID_KEY] = decimal.Decimal(water_volume_obj[Constants.TAG_ID_KEY])
        calculated_document[Constants.TIME_STAMP] = tag_yesterday

        calculated_tag_obj = {
            Constants.TAG_ID_KEY: decimal.Decimal(water_volume_obj[Constants.TAG_ID_KEY]),
            Constants.TIME_STAMP: tag_yesterday,
            Constants.DOCUMENT_KEY: calculated_document
        }
        print('Calculated Tag Object : ', calculated_tag_obj)
        return calculated_tag_obj
    except IndexError as err:
        logger.error("%s - Raw Water Data is Not Found for %s", wa, str(water_intake_tagid))
        raise WaterAnalyticsException(wa + "- Raw Water Data is Not Found for " + str(water_intake_tagid))


def get_water_availability(plant_type, asset_type):
    """
    This functionality is for calculating daily available water in the RWPH.
    The calculation id K04 and tag id is 1002004

    :param plant_type: Raw Water or Clear Water
    :param asset_type: Pump house, etc.
    :return: None
    """
    resource = AWSService(Services.DYNAMO_DB)
    # Getting Plant details and retrieve plant ids based on the region id
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, plant_type, asset_type)
    logger.info("K04 - Plant ID's : %s ", plant_ids)

    # Search for a Source, For that WaterAsset table is used.
    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    for key in plant_ids:
        logger.info("%s - Getting data for Plant ID : %s", wa, key)
        try:
            source_asset = asset_table.query(
                KeyConditionExpression=Key('id').eq(key),
                FilterExpression=Attr('document.attributes.assetType').eq('Tank')
            )
            if source_asset['Items']:
                logger.info('%s - Source Found for Key : %s', wa, key)
                source1 = source_asset['Items'][0]
                document = source1[Constants.DOCUMENT_KEY]
                water_level_intake = document['parameters'][Constants.WATER_LEVEL_AT_INTAKE]
                logger.info('%s - water_level_at_intake ID : %s', wa, water_level_intake[Constants.TAG_ID_KEY])
                water_intake_tagid = int(water_level_intake[Constants.TAG_ID_KEY])
                logger.info('%s - Water Level at intake id %s', wa, water_intake_tagid)
                asset_id = source1[Constants.ASSET_ID_KEY]
                volume_available = document['parameters'][Constants.VOLUME_AVAILABLE]

                # Calling the function where the calculated tags are created
                calculated_tag_obj = get_raw_water_data_and_calculate_tag(resource, water_intake_tagid, asset_id, volume_available)
                water_utilities.save_calculated_water_tag(resource, calculated_tag_obj)
            else:
                logger.debug('%s - Source not found for %s ', wa, key)
                logger.warning('%s - Source not found for %s ', wa, key)
        except WaterAnalyticsException as err:
            logger.error(err)


if __name__ == '__main__':
    get_water_availability('RWPH', 'Pump House')


# 	This functionality is used for calculate the available water on 
# 	daily basis, it has lookup table, where level and volume is defined.
# 	we need to get the valume based on the level value.
# 	The Value should be stored as year on year. i.e. 22 Dec 2017 and 22 Dec 2016.
# 	Calculate based on lookup table and LT01 current value along with YoY comparison

# 	input: tagId of LT01
# 	output: the calculated json with K05 calculatedTag Id
