import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *
from dateutil.relativedelta import relativedelta

logger_file = water_utilities.get_logger_file_name(Constants.RWPH_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_energy_consumption_for_month(resource, tag_id):
    """
    This function get the last month energy consumption data for all days and add all day's values
    and return aggregated monthly energy consumptions
    :param resource:
    :param tag_id:
    :return monthly_energy_consumption:
    """
    last_month_day = datetime.today() - relativedelta(months=1)
    last_month_yyyy_mm = last_month_day.strftime('%Y-%m')
    water_tag_table = resource.get_table(Tables.OPERA_WATER_CALCULATED_TAGS)
    raw_data = water_tag_table.query(
        KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(tag_id) & Key(Constants.TIME_STAMP).begins_with(last_month_yyyy_mm),
    )
    logger.info('K06 - Last Month Energy Consumption Data : %s', raw_data)
    monthly_energy_consumption = 0
    for raw in raw_data['Items']:
        monthly_energy_consumption += decimal.Decimal(raw[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
    logger.debug('K06 - Calculated Monthly Consumption Value : %s', monthly_energy_consumption)

    return monthly_energy_consumption


def calculate_monthly_energy_consumption(plant_type, asset_type):
    """
    This function calculates monthly energy consumptions by adding daily energy consumptions.
    Calculation KPI id : K06 and Tag Id : 1002006
    :param plant_type:
    :param asset_type:
    :return:
    """
    today = datetime.today()
    first_day_of_month = date(today.year, today.month, 1)
    first_day_of_month_timestamp = first_day_of_month.strftime('%Y-%m-%d') + " 00:00:00"

    resource = AWSService(Services.DYNAMO_DB)
    # Getting Plant details and retrieve plant ids based on the region id, plant_type and asset_tye
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, plant_type, asset_type)
    logger.info("K06 - Plant ID's : %s", plant_ids)

    for plant in plant_ids:
        try:
            plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
            query_data = plant_table.get_item(
                Key={
                    'id': Region.R1, Constants.PLANT_ID_KEY: plant
                }
            )
            if query_data['Item']:
                plant_data = query_data['Item']
                print('Plant Data : ', plant_data)
                parameters = plant_data[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
                pc = parameters[Constants.ENERGY_CONSUMPTION_PER_DAY]
                pcm = parameters[Constants.ENERGY_CONSUMPTION_PER_MONTH]
                monthly_consumption = get_energy_consumption_for_month(resource, decimal.Decimal(pc[Constants.TAG_ID_KEY]))

                monthly_object = water_utilities.create_calculated_object(
                    pcm[Constants.TAG_ID_KEY], monthly_consumption, first_day_of_month_timestamp)
                water_utilities.save_calculated_water_tag(resource, monthly_object)
            else:
                logger.debug("K06 - NO Plant Data Found for plant ID %s", plant)
                logger.warning("K06 - NO Plant Data Found for plant ID %s", plant)
                raise WaterAnalyticsException("NO Plant Data Found for plant ID " + plant)
        except WaterAnalyticsException as err:
            logger.error(err)


if __name__ == '__main__':
    calculate_monthly_energy_consumption('RWPH', 'Pump House')