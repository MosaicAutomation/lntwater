import sys
import logging
from datetime import datetime, timedelta
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)

WTP_L = 25.3
WTP_B = 7.5


def get_volume_of_backwash_water_level(resource, plant_id, source_type, day):
    try:
        asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
        source_assets = asset_table.query(
            KeyConditionExpression=Key('id').eq(plant_id),
            FilterExpression=Attr('document.attributes.tankModel').eq(source_type)
        )
        for source in source_assets['Items']:
            print("Source : ", source[Constants.ASSET_ID_KEY])
            b_parameters = source[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
            bwt_level_tag = b_parameters[Constants.WTP_BACKWASH_TANK_LEVEL]
            volume_in_backwash = b_parameters[Constants.WTP_VOLUME_AVAILABLE_IN_BACKWASH_TANK]
            data = water_utilities.get_raw_opera_water_data(resource, decimal.Decimal(bwt_level_tag[Constants.TAG_ID_KEY]), day)
            value = data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
            logger.info("K11 - The Value of tag '%s' for the day %s is %s", bwt_level_tag[Constants.TAG_ID_KEY], day, value)
            volume = WTP_B * WTP_L * float(value)
            logger.info("K11 - Volume of backwash tank water is %s ", volume)
            return volume_in_backwash[Constants.TAG_ID_KEY], volume
    except Exception as err:
        logger.error("K11 - %s", err)
        raise WaterAnalyticsException("Error with Calculating Average Value of backwash water level")


def calculate_volume_of_backwash_tank(plant_type, asset_type, source_asset_type):
    """
    This function calculates the volume of water in backwash tank.
    The formula is Volume = L*B*H where L = 25.3 and B = 7.5 are Constants.
    H is value need to take from LT01 tag

    :param plant_type:
    :param asset_type:
    :param source_asset_type:
    :return:
    """
    # V=25.3*7.5*H

    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    yesterday_date = str_yesterday + " 00:00:00"

    resource = AWSService(Services.DYNAMO_DB)
    # Getting Plant details and retrieve plant ids based on the region id, plant_type and asset_tye
    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(Region.R1),
        FilterExpression=Attr('document.plantId').contains(plant_type) & Attr('document.attributes.assetType').contains(
            asset_type)
    )
    for plant in plants['Items']:
        try:
            plant_id = plant[Constants.PLANT_ID_KEY]
            print("Plant ID : ", plant_id)
            volume_tag, volume_value = get_volume_of_backwash_water_level(resource, plant_id, source_asset_type, str_yesterday)
            obj = water_utilities.create_calculated_object(volume_tag, round(volume_value, 2), yesterday_date)
            print("Final Object : ", obj)
            # water_utilities.save_calculated_water_tag(resource, obj)
            logger.info("K11 - VOLUME AVAILABLE IN BACKWASH TANK is Saved %s", obj)
        except WaterAnalyticsException as err:
            logger.error("K11 - %s", err)
        except Exception as err:
            logger.error("K11 - Big Error - %s", err)


if __name__ == '__main__':
    calculate_volume_of_backwash_tank('WTP', 'Treatment Plant', 'Backwash Tank')