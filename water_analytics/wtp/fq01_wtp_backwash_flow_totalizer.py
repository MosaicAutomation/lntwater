import sys
from os import path
# sys.path.append("D:\MB\Final Version\lnt-water-analytics\\")
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *
from datetime import datetime, timedelta
import decimal
import logging
from boto3.dynamodb.conditions import Key, Attr

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_cumulative_flow_rate(resource, tag_id, day):
    try:
        final_value = 0
        water_tag_table = resource.get_table(Tables.OPERA_WATER_TAGS)
        raw_data = water_tag_table.query(
            KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(tag_id) & Key(Constants.TIME_STAMP).begins_with(day),
        )
        for raw in raw_data['Items']:
            value = float(raw[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
            final_value += value
        return final_value
    except IndexError as err:
        logger.error('FQ01 - BACKWASH FLOW TOTALIZER - No Data found for tag id % and for date %s', str(tag_id), day)
        return 0


def calculate_flow_totalizer_for_backwash_tank(plant_type, asset_type, tank_model):
    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    resource = AWSService(Services.DYNAMO_DB)
    # Getting Plant details and retrieve plant ids based on the region id
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, plant_type, asset_type)

    # Search for a Source, For that WaterAsset table is used.
    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    for key in plant_ids:
        logger.info("FQ01 - BACKWASH FLOW TOTALIZER - Getting data for Plant ID : %s", key)
        tanks = asset_table.query(
            KeyConditionExpression=Key('id').eq(key),
            FilterExpression=Attr('document.attributes.tankModel').eq(tank_model)
        )
        for tank in tanks['Items']:
            try:
                parameters = tank[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
                flow_rate = parameters[Constants.WTP_BACKWASH_WATER_FLOW_RATE][Constants.TAG_ID_KEY]
                flow_totalizer = parameters[Constants.WTP_BACKWASH_TANK_WATER_FLOW_TOTALIZER][Constants.TAG_ID_KEY]
                logger.info('FQ01 - BACKWASH FLOW TOTALIZER - Flow Rate : %s,  Flow Totalizer : %s', flow_rate, flow_totalizer)
                cumulative_value = get_cumulative_flow_rate(resource, decimal.Decimal(flow_rate), str_yesterday)
                final_obj = water_utilities.create_calculated_object(
                    flow_totalizer, cumulative_value, str_yesterday + " 00:00:00")
                water_utilities.save_calculated_water_tag(resource, final_obj)
                logger.info('FQ01 - BACKWASH FLOW TOTALIZER - Saved Calculated Object : %s', final_obj)
            except WaterAnalyticsException as err:
                logger.error('FQ01 - BACKWASH FLOW TOTALIZER - %s', err)
            except Exception as err:
                logger.error('FQ01 - BACKWASH FLOW TOTALIZER - %s', err)


if __name__ == '__main__':
    calculate_flow_totalizer_for_backwash_tank('WTP', 'Treatment Plant', 'Backwash Tank')