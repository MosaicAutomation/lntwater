import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def create_calculated_tag(tag_id, final_value, day):
    calculated_document = {
        Constants.TAG_ID_KEY: decimal.Decimal(tag_id),
        Constants.TAG_VALUE_KEY: final_value,
        Constants.TIME_STAMP: day
    }
    calculated_tag_obj = {
        Constants.TAG_ID_KEY: decimal.Decimal(tag_id),
        Constants.TIME_STAMP: day,
        Constants.DOCUMENT_KEY: calculated_document
    }
    return calculated_tag_obj


def calculate_wtp_k18(plant_type, asset_type):
    """
    This function calculate the Unit consumption of Alum for supplying MLD of water.

    :param plant_type:
    :param asset_type:
    :return:
    """
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    yesterday_date = str_yesterday + " 00:00:00"

    resource = AWSService(Services.DYNAMO_DB)
    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(Region.R1),
        FilterExpression=Attr('document.plantId').contains(plant_type) & Attr('document.attributes.assetType').contains(
            asset_type)
    )
    for plant in plants['Items']:
        try:
            parameters = plant[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
            k01 = parameters[Constants.WTP_K01_WATER_PROCESSED_PER_DAY][Constants.TAG_ID_KEY]
            lime_tag_key = parameters[Constants.WTP_LIME_CONSUMPTION][Constants.TAG_ID_KEY]
            k01_data = water_utilities.get_calculated_opera_water_data(resource, decimal.Decimal(k01), str_yesterday)
            k01_value = decimal.Decimal(k01_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])

            try:
                k18 = parameters[Constants.WTP_K18_UNIT_LIME_CONSUMPTION][Constants.TAG_ID_KEY]
                alum_data = water_utilities.get_raw_opera_water_data(
                    resource, decimal.Decimal(lime_tag_key), str_yesterday)
                alum_value = decimal.Decimal(alum_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
                logger.info('Value of %s is %s and Value of %s is %s', k01, k01_value, lime_tag_key, alum_value)
                k18_value = alum_value / k01_value
                final_obj = water_utilities.create_calculated_object(k18, round(k18_value, 3), yesterday_date)
                print("Final Object : ", final_obj)
                water_utilities.save_calculated_water_tag(resource, final_obj)
            except WaterAnalyticsException as err:
                logger.error('K18 - Error Occurred in K18 Calculations : %s', err)
        except WaterAnalyticsException as err:
            logger.error(err)
        except Exception as err:
            logger.error(err)


if __name__ == '__main__':
    calculate_wtp_k18('WTP', 'Treatment Plant')
