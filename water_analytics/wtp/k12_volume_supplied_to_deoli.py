import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def calculate_volume_supplied_to_deoli():
    """
    This function calculate the volume of water supplied to deoli.
    :return:
    """
    resource = AWSService(Services.DYNAMO_DB)
    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    cwph01 = asset_table.get_item(
        Key={'id': 'WET:IN:RAJ:BSL:74MLD:CWPH01', 'assetId': 'WET:IN:RAJ:BSL:74MLD:CWPH01:HDRL02'}
    )
    cwph01_item = cwph01['Item']
    parameters = cwph01_item[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
    fq03_tag = parameters[Constants.FLOW_TOTALIZER]
    fq03_tag_id = fq03_tag[Constants.TAG_ID_KEY]
    logger.info("K12 - Source %s and Flow Totalizer %s", cwph01_item[Constants.ASSET_ID_KEY], fq03_tag_id)
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    try:
        fq03_data = water_utilities.get_raw_opera_water_data(resource, decimal.Decimal(fq03_tag_id), str_yesterday)
        fq03_value = fq03_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
        logger.info("K12 - Data Found for Flow Totalizer %s is %s", fq03_tag_id, fq03_data)
        plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)

        wtp_plant = plant_table.get_item(
           Key={'id': Region.R1, 'plantId': 'WET:IN:RAJ:BSL:74MLD:WTP01'}
        )

        logger.info('WTP PLANT :%s ', wtp_plant)
        wtp_parameters = wtp_plant['Item'][Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        wtp_k12_tag_id = wtp_parameters[Constants.VOLUME_SUPPLIED_TO_DEOLI_PER_DAY][Constants.TAG_ID_KEY]

        yesterday = str_yesterday + " 00:00:00"
        final_obj = water_utilities.create_calculated_object(wtp_k12_tag_id, fq03_value, yesterday)
        logger.info('Calculated Object : %s', final_obj)
        water_utilities.save_calculated_water_tag(resource, final_obj)
    except WaterAnalyticsException as err:
        logger.error(err)


if __name__ == '__main__':
    calculate_volume_supplied_to_deoli()
