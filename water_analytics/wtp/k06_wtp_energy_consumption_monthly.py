import sys
from datetime import datetime, timedelta
import decimal
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *
from dateutil.relativedelta import relativedelta


logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_monthly_consumed_energy(resource, tag_id):
    last_month_day = datetime.today() - relativedelta(months=1)
    last_month_yyyy_mm = last_month_day.strftime('%Y-%m')
    water_tag_table = resource.get_table(Tables.OPERA_WATER_CALCULATED_TAGS)
    raw_data = water_tag_table.query(
        KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(tag_id) & Key(Constants.TIME_STAMP).begins_with(
            last_month_yyyy_mm),
    )
    monthly_consumed_energy = 0
    for raw in raw_data['Items']:
        monthly_consumed_energy += decimal.Decimal(raw[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
    return monthly_consumed_energy


def calculate_aggregate_energy_consumed_per_month(plant_type):
    today = datetime.today()
    first_day_of_month = date(today.year, today.month, 1)
    first_day_of_month_timestamp = first_day_of_month.strftime('%Y-%m-%d') + " 00:00:00"

    resource = AWSService(Services.DYNAMO_DB)

    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    wtp_plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(Region.R1),
        FilterExpression=Attr('document.plantId').contains(plant_type)
    )
    for plant in wtp_plants['Items']:
        parameters = plant[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        consumed_per_day = parameters[Constants.WTP_K05_ENERGY_CONSUMPTION_PER_DAY][Constants.TAG_ID_KEY]
        consumed_per_month = parameters[Constants.ENERGY_CONSUMPTION_PER_MONTH][Constants.TAG_ID_KEY]
        logger.info("WTP - K06 - MONTHLY ENERGY CONSUMPTION - consumed_per_day ID : %s  | consumed_per_month ID %s",
                    consumed_per_day, consumed_per_month)

        monthly_volume = get_monthly_consumed_energy(resource, decimal.Decimal(consumed_per_day))

        logger.info("WTP - K06 - MONTHLY ENERGY CONSUMPTION - Monthly Volume : ", monthly_volume)

        calculated_obj = water_utilities.create_calculated_object(
           consumed_per_month, round(monthly_volume, 2), first_day_of_month_timestamp)
        water_utilities.save_calculated_water_tag(resource, calculated_obj)
        logger.info("WTP - K06 - MONTHLY ENERGY CONSUMPTION - Calculated Object Saved")


if __name__ == '__main__':
    calculate_aggregate_energy_consumed_per_month('WTP')
