import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def calculate_volume_supplied_to_tonk():
    resource = AWSService(Services.DYNAMO_DB)

    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)

    cwph01 = asset_table.get_item(
        Key={'id': 'WET:IN:RAJ:BSL:74MLD:CWPH02', 'assetId': 'WET:IN:RAJ:BSL:74MLD:CWPH02:HDRL02'}
    )
    print("CWPH01  : ", cwph01['Item'])
    cwph01_item = cwph01['Item']
    parameters = cwph01_item[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
    fq03_tag = parameters[Constants.FLOW_TOTALIZER]
    print('FQ03 TAG - ', fq03_tag)

    fq03_tag_id = fq03_tag[Constants.TAG_ID_KEY]
    logger.info('FQ03 TAG id : %s', fq03_tag_id)

    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    try:
        fq03_data = water_utilities.get_raw_opera_water_data(resource, decimal.Decimal(fq03_tag_id), str_yesterday)
        fq03_value = fq03_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
        plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
        wtp_plant = plant_table.get_item(
           Key={'id': Region.R1, 'plantId': 'WET:IN:RAJ:BSL:74MLD:WTP01'}
        )

        logger.info('WTP PLANT : %s', wtp_plant)
        wtp_parameters = wtp_plant['Item'][Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        wtp_k13_tag_id = wtp_parameters[Constants.VOLUME_SUPPLIED_TO_TONK_PER_DAY][Constants.TAG_ID_KEY]
        print('wtp_k13 : ', wtp_k13_tag_id)

        yesterday = str_yesterday + " 00:00:00"
        final_obj = water_utilities.create_calculated_object(wtp_k13_tag_id, fq03_value, yesterday)
        print('Calculated Object : ', final_obj)
        water_utilities.save_calculated_water_tag(resource, final_obj)
    except WaterAnalyticsException as err:
        print(err)


if __name__ == '__main__':
    calculate_volume_supplied_to_tonk()
