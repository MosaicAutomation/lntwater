import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_average_turbidity_value(resource, tag_id, day):
    try:
        final_value = 0
        water_tag_table = resource.get_table(Tables.OPERA_WATER_TAGS)
        raw_data = water_tag_table.query(
            KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(tag_id) & Key(Constants.TIME_STAMP).begins_with(day),
        )
        for raw in raw_data['Items']:
            print('RAW - ', raw)
            value = float(raw[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
            final_value += value

        logger.info("Final Calculated Value is %s and Record Count is %s ", final_value, raw_data['Count'])

        avg = final_value / raw_data['Count']
        return avg
    except IndexError as err:
        logger.error('No Data found for tag id % and for date %s - Err : %s', str(tag_id), day, err)
        return 0
    except ZeroDivisionError as err:
        logger.error('Error Due to Zero Record Found for tag id %s and date %s - Err: %s', tag_id, day, err)
        return 0


def calculate_daily_average_turbidity(plant_type, asset_type):
    """
    This function is calculating the average of daily turbidity.
    :param plant_type:
    :param asset_type:
    :return:
    """
    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    resource = AWSService(Services.DYNAMO_DB)
    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(Region.R1),
        FilterExpression=Attr('document.plantId').contains(plant_type) & Attr('document.attributes.assetType').contains(
            asset_type)
    )
    for plant in plants['Items']:
        parameters = plant[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        average_turbidity_tag = parameters[Constants.WTP_VOLUME_AVG_TURBIDITY_BEFORE_FILTRATION]
        turbidity_value_tag = parameters[Constants.WTP_TURBIDITY_VALUE_BEFORE_FILTRATION]
        average_value = get_average_turbidity_value(
            resource, decimal.Decimal(turbidity_value_tag[Constants.TAG_ID_KEY]), str_yesterday)
        logger.info('Average Turbidity Value : %s', average_value)
        final_obj = water_utilities.create_calculated_object(
            average_turbidity_tag[Constants.TAG_ID_KEY], round(average_value, 2), str_yesterday + " 00:00:00")
        water_utilities.save_calculated_water_tag(resource, final_obj)


if __name__ == '__main__':
    calculate_daily_average_turbidity('WTP', 'Treatment Plant')