import sys
from datetime import datetime, timedelta
import logging
# sys.path.append("D:\MB\Final Version\lnt-water-analytics\\")
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.CWPH_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_calculated_unit_value(resource, plant_id, source_type, day):
    try:
        asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
        source_assets = asset_table.query(
            KeyConditionExpression=Key('id').eq(plant_id),
            FilterExpression=Attr('document.attributes.assetType').eq(source_type)
        )
        processed_value = 0
        for source in source_assets['Items']:
            value = 0
            logger.info("K13 - Getting Processed Water Data for %s of plant %s", source[Constants.ASSET_ID_KEY], plant_id)
            try:
                s_parameters = source[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
                processed_tag = s_parameters[Constants.VOLUME_SUPPLIED_PER_DAY]
                r_data = water_utilities.get_calculated_opera_water_data(
                    resource, decimal.Decimal(processed_tag[Constants.TAG_ID_KEY]), day)
                value = r_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
            except KeyError as key_err:
                logger.info("K13 - Key %s is not Available in ASSET '%s' ", key_err, source[Constants.ASSET_ID_KEY])
            except WaterAnalyticsException as err:
                logger.error("K13 - %s", err)

            processed_value += float(value)
        return processed_value
    except Exception as err:
        logger.error("K13 - E : %s", err)
    return 0


def calculate_energy_consumption_for_water_supply(plant_type, asset_type, source_asset_type):
    """
    This function is used for calculating energy consumption of a plant on daily basis.
    The Calculation id is K13 - tag id 1002113.
    It has a formula
    Consumption = K10 / (K02 + K03)
    :param plant_type: plant type can be RWPH or CWPH
    :param asset_type: asset type can be Pump House, etc.
    :param source_asset_type: It will be type Pipe
    :return None:
    """
    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    yesterday_date = str_yesterday + " 00:00:00"

    resource = AWSService(Services.DYNAMO_DB)
    # Getting Plant details and retrieve plant ids based on the region id, plant_type and asset_tye
    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(Region.R1),
        FilterExpression=Attr('document.plantId').contains(plant_type) & Attr('document.attributes.assetType').contains(
            asset_type)
    )
    for plant in plants['Items']:
        try:
            plant_tag_id = plant[Constants.PLANT_ID_KEY]
            parameters = plant[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
            per_day_tag = parameters[Constants.ENERGY_CONSUMPTION_PER_DAY]
            per_mld_tag = parameters[Constants.ENERGY_CONSUMPTION_PER_MLD]
            unit_value = get_calculated_unit_value(resource, plant_tag_id, source_asset_type, str_yesterday)
            logger.info("K13 - The Processed Value(K06 + K07) for plant %s is %s", plant_tag_id, unit_value)
            logger.info("K13 - Getting Value of energy_consumption_per_day(%s) for plant %s", per_day_tag[Constants.TAG_ID_KEY], plant_tag_id)
            try:
                per_day_data = water_utilities.get_calculated_opera_water_data(
                    resource, decimal.Decimal(per_day_tag[Constants.TAG_ID_KEY]), str_yesterday)
                logger.info("Value of energy_consumption_per_day(%s) is %s", per_day_tag[Constants.TAG_ID_KEY],
                            per_day_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
                final_value = float(per_day_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])/unit_value
                logger.info("K13 - Calculated Value of ENERGY CONSUMPTION PER MLD is %s", final_value)
                obj = water_utilities.create_calculated_object(
                    per_mld_tag[Constants.TAG_ID_KEY], round(final_value, 2), yesterday_date
                )
                logger.info("K13 - Object Value for %s is %s", per_mld_tag[Constants.TAG_ID_KEY], obj)
                water_utilities.save_calculated_water_tag(resource, obj)
            except WaterAnalyticsException as err:
                logger.error("K13 - %s", err)

        except WaterAnalyticsException as err:
            logger.error("K13 - %s", err)
        except Exception as err:
            print("BIG E : ", err)
            logger.error("K13 - BIG E : %s", err)


if __name__ == '__main__':
    calculate_energy_consumption_for_water_supply('CWPH', 'Pump House', 'Pipe')