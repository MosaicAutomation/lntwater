import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *


logger_file = water_utilities.get_logger_file_name(Constants.INCIDENT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_alarm_description(resource, tag_id, bit_number):
    try:
        alarm_tag_table = resource.get_table(Tables.OPERA_WATER_ALARM)
        raw_data = alarm_tag_table.get_item(Key={'tagId': tag_id, 'bitNumber': bit_number})
        raw_water_data = raw_data['Item']
        return raw_water_data
    except KeyError as err:
        print(err)
        logger.error('ALARM - Alarm details for tag %s  not found - %s', tag_id, err)


def save_alarm_incidents(resource, raw_alarm, bit_positions):
    for bit in bit_positions:
        try:
            alarm_data = get_alarm_description(resource, raw_alarm[Constants.TAG_ID_KEY], bit)
            incident_data = {
                Constants.PLANT_ID_KEY: alarm_data[Constants.DOCUMENT_KEY][Constants.PLANT_ID_KEY],
                'timeStamp': raw_alarm[Constants.TIME_STAMP],
                Constants.DOCUMENT_KEY: {
                    Constants.ASSET_ID_KEY: alarm_data[Constants.DOCUMENT_KEY][Constants.ASSET_ID_KEY],
                    'type': alarm_data[Constants.DOCUMENT_KEY]['type'],
                    Constants.ASSET_NAME: alarm_data[Constants.DOCUMENT_KEY][Constants.ASSET_NAME],
                    'description': alarm_data[Constants.DOCUMENT_KEY]['description'],
                    Constants.REGION_ID_KEY: alarm_data[Constants.DOCUMENT_KEY][Constants.REGION_ID_KEY]
                }
            }
            print('INCIDENT OBJECT : ', incident_data)
            # water_utilities.save_calculated_incident(resource, incident_data)
        except WaterAnalyticsException as err:
            logger.error("ALARM - %s", err)

    return 0


def convert_into_binary_and_get_bit_positions(num):
    bits = format(num, '016b')
    logger.info("ALARM - bit format for %s is %s - Checking for all bit", num, bits)
    positions = [i for i, value in enumerate(bits[::-1]) if value == '1']
    return positions


def decode_and_save_alarm_data(resource, alarm_raw_data):
    for raw_water in alarm_raw_data:
        value = raw_water[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
        bit_positions = convert_into_binary_and_get_bit_positions(int(value))
        save_alarm_incidents(resource, raw_water, bit_positions)


def get_raw_water_data_for_alarm(resource, tag_id, day):
    water_tag_table = resource.get_table(Tables.OPERA_WATER_TAGS)
    raw_data = water_tag_table.query(
        KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(tag_id) & Key(Constants.TIME_STAMP).begins_with(day),
        ScanIndexForward=False,
    )
    if raw_data['Items']:
        return raw_data['Items']
    else:
        logger.info('ALARM - No alarm for %s and %s ', tag_id, day)


def check_alarm_for_0th_bit(resource, tag_list, day):
    for tag_id in tag_list:
        print('For Tag : ', tag_id)
        try:
            alarm_raw_data = get_raw_water_data_for_alarm(resource, decimal.Decimal(tag_id), day)
            for raw_water in alarm_raw_data:
                value = raw_water[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
                bit_format = format(int(value), '016b')
                logger.info("ALARM - bit format for %s is %s - Checking for 0th position bit", value, bit_format)
                if bit_format[-1] == '0':
                    save_alarm_incidents(resource, raw_water, [0])
        except WaterAnalyticsException as err:
            logger.error("ALARM - %s", err)
        except Exception as err:
            logger.error("ALARM - %s", err)


def check_alarm_for_all_bit(resource, tag_list, day):
    """
    This function is used for checking all the bit of the alarm value.
    :param resource:
    :param tag_list:
    :param day:
    :return:
    """
    for tag_id in tag_list:
        try:
            alarm_raw_data = get_raw_water_data_for_alarm(resource, decimal.Decimal(tag_id), day)
            decode_and_save_alarm_data(resource, alarm_raw_data)
        except WaterAnalyticsException as err:
            logger.error("ALARM - %s", err)
        except Exception as err:
            logger.error("ALARM - %s", err)


def check_for_daily_alarms(resource, plant_id):
    """
    This function is working on the daily alarms; it gets data from OperaWaterTag table.
    It generate bit format for the value and check every bit against AlarmTable
    :return:
    """

    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")
    alarm_lookup_table = resource.get_table(Tables.OPERA_WATER_ALARM_LOOKUP)
    response = alarm_lookup_table.get_item(
        Key={Constants.REGION_ID_KEY: Region.R1, Constants.PLANT_ID_KEY:plant_id}
    )
    print("Response : ", response, "Plant ID : ", plant_id)
    if response["Item"]:
        ALL_BIT_TAGS = response['Item'][Constants.DOCUMENT_KEY]['ALL_BIT_TAGS']
        ZEROTH_BIT_TAGS = response['Item'][Constants.DOCUMENT_KEY]['ZEROTH_BIT_TAGS']
        # Check for All bit Alarms
        try:
            check_alarm_for_all_bit(resource, ALL_BIT_TAGS, str_yesterday)
        except Exception as err:
            logger.error("ALARM - %s", err)

        # Check for Zero Bit Alarms
        try:
            check_alarm_for_0th_bit(resource, ZEROTH_BIT_TAGS, str_yesterday)
        except Exception as err:
            logger.error("ALARM - %s", err)
    else:

        print("NO Alarm Lookup found for Plant : ", plant_id)


if __name__ == '__main__':
    resource = AWSService(Services.DYNAMO_DB)
    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    plants = plant_table.scan()
    for plant in plants['Items']:
        try:
            check_for_daily_alarms(resource, plant[Constants.PLANT_ID_KEY])
        except Exception as err:
            print("Error : ", err)
            logger.error("DAILY ALARM - Error : %s for %s", err, plant[Constants.PLANT_ID_KEY])

