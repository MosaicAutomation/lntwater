import boto3


# Get the service resource.
class AWSService():
    """docstring for AWService"""

    def __init__(self, source_name):
        super(AWSService, self).__init__()
        self.resource = boto3.resource(source_name)

    def get_aws_resource(self):
        return self.resource

    def get_table(self, table_name):
        return self.resource.Table(table_name)




