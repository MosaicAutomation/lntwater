import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_flow_rate_efficiency(resource, fltr_name, tag_id, day):
    """
    his function calculates the Flow Rate efficiency of filter.
    The Value has taken as 0.015 is taken by 30 divided by the value of the difference between 2000 and 0.
    30 is the highest percentage that flow rate can have in filter efficiency.
    :param resource:
    :param fltr_name:
    :param tag_id:
    :param day:
    :return:
    """
    try:
        raw_data = water_utilities.get_raw_opera_water_data(resource, decimal.Decimal(tag_id), day)
        value = decimal.Decimal(raw_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
        if value > 2000:
            return decimal.Decimal(30)
        else:
            return decimal.Decimal(value * 0.015)
    except WaterAnalyticsException as err:
        logger.error('FILTER EFFICIENCY - %s', err)
        return decimal.Decimal(0)


def get_running_hrs_efficiency(resource, fltr_name, tag_id, day):
    """
    his function calculates the filter running hours efficiency of filter.
    The Value has taken as 3.75 is taken by 30 divided by the value of the difference between 8 and 16.
    30 is the highest percentage that Filter Running hours can have in filter efficiency.
    :param resource:
    :param fltr_name:
    :param tag_id:
    :param day:
    :return:
    """
    try:
        raw_data = water_utilities.get_raw_opera_water_data(resource, decimal.Decimal(tag_id), day)
        value = decimal.Decimal(raw_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
        if 0 < value < 8:
            return decimal.Decimal(30)
        elif 8 < value < 16:
            return decimal.Decimal(30 - ((float(value) - 8) * 3.75))
        else:
            return decimal.Decimal(0)
    except WaterAnalyticsException as err:
        logger.error('FILTER EFFICIENCY - %s', err)
        return decimal.Decimal(0)


def get_bed_level_efficiency(resource, fltr_name, tag_id, day):
    """
    This function calculates the bed level efficiency of filter.
    The Value has taken as 28.57 is taken by 40 divided by the value of the difference between 0.2 and 1.6.
    40 is the highest percentage that bed level can have in filter efficiency.
    :param resource:
    :param fltr_name:
    :param tag_id:
    :param day:
    :return:
    """
    try:
        raw_data = water_utilities.get_raw_opera_water_data(resource, decimal.Decimal(tag_id), day)
        value = decimal.Decimal(raw_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
        if 0 < value < 0.2:
            return decimal.Decimal(40)
        elif 0.2 < value < 1.6:
            return decimal.Decimal(40 - ((float(value) - 0.2) * 28.57))
        else:
            return decimal.Decimal(0)
    except WaterAnalyticsException as err:
        logger.error('FILTER EFFICIENCY - %s', err)
        return decimal.Decimal(0)


def wtp_calculate_filter_efficiency(plant_type, asset_type, source_asset_type):
    """
    This function calculates the filter efficiency. The frequency is Every 15 min.
    The Filter efficiency is divided into 3 Parts:
        1. Flow Rate Efficiency - 30%
        2. InBedLevel Efficiency - 40%
        3. Number of Hours Running - 30%

    :param plant_type:
    :param asset_type:
    :param source_asset_type:
    :return:
    """
    today_date = datetime.now().strftime("%Y-%m-%d")

    resource = AWSService(Services.DYNAMO_DB)
    # Getting Plant details and retrieve plant ids based on the region id
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, plant_type, asset_type)
    logger.info("FILTER EFFICIENCY - Plant ID's : %s ", plant_ids)

    # Search for a Source, For that WaterAsset table is used.
    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    for key in plant_ids:
        logger.info("FILTER EFFICIENCY - Getting data for Plant ID : %s", key)
        try:
            filters = asset_table.query(
                KeyConditionExpression=Key('id').eq(key),
                FilterExpression=Attr('document.attributes.assetType').eq(source_asset_type)
            )
            for wtp_filter in filters['Items']:
                filter_name = wtp_filter[Constants.ASSET_ID_KEY]
                logger.info('FILTER EFFICIENCY - Calculating efficiency for filter - %s', filter_name)
                parameters = wtp_filter[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]

                flow_rate = parameters[Constants.WTP_FILTER_FLOW_RATE][Constants.TAG_ID_KEY]
                in_bed_level = parameters[Constants.WTP_FILTER_BED_LEVEL][Constants.TAG_ID_KEY]
                no_of_running_hrs = parameters[Constants.WTP_NUMBER_OF_RUNNING_HOURS_PER_DAY][Constants.TAG_ID_KEY]
                filter_efficiency_tag = parameters[Constants.WTP_FILTER_EFFICIENCY][Constants.TAG_ID_KEY]
                try:
                    fl_efficiency = get_flow_rate_efficiency(resource, filter_name, flow_rate, today_date)
                    bed_efficiency = get_bed_level_efficiency(resource, filter_name, in_bed_level, today_date)
                    run_efficiency = get_running_hrs_efficiency(resource, filter_name, no_of_running_hrs, today_date)
                    fltr_efficiency = fl_efficiency + run_efficiency + bed_efficiency
                    logger.info("FILTER EFFICIENCY - Filter Efficiency = %s", fltr_efficiency)
                    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    obj = water_utilities.create_calculated_object(filter_efficiency_tag, round(fltr_efficiency, 2), current_time)
                    water_utilities.save_calculated_water_tag(resource, obj)
                    logger.info("FILTER EFFICIENCY - Saved Object %s ", obj)
                except WaterAnalyticsException as err:
                    print(err)
                    logger.error(err)
        except WaterAnalyticsException as err:
            print("FILTER EFFICIENCY - %s", err)
        except Exception as err:
            print("FILTER EFFICIENCY - %s", err)


if __name__ == '__main__':
    wtp_calculate_filter_efficiency('WTP', 'Treatment Plant', 'Filter')
