import sys
from datetime import datetime, timedelta
import logging
# sys.path.append("D:\MB\Final Version\lnt-water-analytics\\")
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.CWPH_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_raw_water_data_and_calculate_tag(resource, pump_data):
    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    today_date = today_date + " 00:00:00"
    # Getting Raw Data from OperaWaterTag table
    water_tag_table = resource.get_table(Tables.OPERA_WATER_TAGS)
    # Getting Recent Data based on yesterday date
    logger.info('PRHD - KWARGS : %s', pump_data)
    print('Running Hours Key : ', pump_data[Constants.RUNNING_HOURS])
    running_hours_key = decimal.Decimal(pump_data[Constants.RUNNING_HOURS][Constants.TAG_ID_KEY])
    try:
        raw_data = water_tag_table.query(
            KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(running_hours_key)
            & Key(Constants.TIME_STAMP).begins_with(str_yesterday),
            ScanIndexForward=False,
            Limit=1
        )
        # print('RAW DATA : ', raw_data)
        hours_data = raw_data['Items'][0]
        logger.info('PRHD - Raw Power Data Found for power consumption id %s and date %s - %s', running_hours_key, str_yesterday, hours_data)
        tag_value = hours_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]

        tag_yesterday = str_yesterday + " 00:00:00"

        calculated_document = dict()
        calculated_document[Constants.TAG_VALUE_KEY] = str(tag_value)
        calculated_document[Constants.TAG_ID_KEY] = decimal.Decimal(
            pump_data[Constants.PUMP_RUNNING_HOURS_PER_DAY][Constants.TAG_ID_KEY])
        calculated_document[Constants.TIME_STAMP] = tag_yesterday

        calculated_tag_obj = {
            Constants.TAG_ID_KEY: decimal.Decimal(
                pump_data[Constants.PUMP_RUNNING_HOURS_PER_DAY][Constants.TAG_ID_KEY]),
            Constants.TIME_STAMP: tag_yesterday,
            Constants.DOCUMENT_KEY: calculated_document
        }
        return calculated_tag_obj
    except IndexError as err:
        logger.error('PRHD - %s for Tag Id %s ', err, str(running_hours_key))
        raise WaterAnalyticsException("PRHD - Raw Power Data is Not Found for " + str(running_hours_key))


def calculate_running_hours_for_all_pumps(plant_type, asset_type):
    """
    This function calculates the daily running hours for all pumps individually.
    i.e. running hours per day for each pump
    KPI ID:	P01RHD	P02RHD	P03RHD	P04RHD	P05RHD	P06RHD	P07RHD	P08RHD	P09RHD	P10RHD
    TAG ID:	1002117	1002118	1002119	1002120	1002121	1002122	1002123	1002124	1002125	1002126


    :param plant_type:
    :param asset_type:
    :return: None
    """
    resource = AWSService(Services.DYNAMO_DB)
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, plant_type, 'Pump House')
    logger.info('PRHD - Getting data for plants : %s', plant_ids)
    # Search for a Source, For that WaterAsset table is used.
    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    asset_list = []
    for plant_key in plant_ids:
        source_assets = asset_table.query(
            KeyConditionExpression=Key('id').eq(plant_key),
            FilterExpression=Attr('document.parentId').contains(plant_type) & Attr('document.attributes.assetType').eq(asset_type)
        )
        print('For Plant : ', plant_key, '  Source Count: ', source_assets['Count'])
        logger.info('PRHD - For Plant : %s, %s source items found ', plant_key, source_assets['Count'])

        for sa in source_assets['Items']:
            # assetId, runningHours, pumpRunningHoursPerDay
            parameters = sa[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
            asset = {
                Constants.ASSET_ID_KEY: sa[Constants.ASSET_ID_KEY],
                Constants.RUNNING_HOURS: parameters[Constants.RUNNING_HOURS],
                Constants.PUMP_RUNNING_HOURS_PER_DAY: parameters[Constants.PUMP_RUNNING_HOURS_PER_DAY]
            }
            asset_list.append(asset)
    # Calculate daily running hours for each pump
    for asset in asset_list:
        try:
            logger.info('PRHD - Calculating Running Hours for %s', asset[Constants.ASSET_ID_KEY])
            calculated_object = get_raw_water_data_and_calculate_tag(resource, asset)
            water_utilities.save_calculated_water_tag(resource, calculated_object)
            logger.info('PRHD - Calculated Tag for Running hours pump : %s Saved', asset[Constants.ASSET_ID_KEY])
        except WaterAnalyticsException as err:
            logger.error(err)


if __name__ == '__main__':
    calculate_running_hours_for_all_pumps('CWPH', 'Pump')