import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.RWPH_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)
uce = "UNIT_CONSUMPTION_OF_ENERGY_PER_MLD"


def get_calculated_tag_value(resource, tag_id, day):
    """
    This function get the value of tag_id from OperaWaterCalculatedTags table.

    :param resource: The AWS service, we are using here DynamoDB
    :param tag_id: tag_id for which we need to get data
    :param day: the date in YYYY-MM-DD format
    :return: value: the value of the tag id
    """
    # day = '2017-12-10'
    try:
        calculated_tag_table = resource.get_table(Tables.OPERA_WATER_CALCULATED_TAGS)
        raw_data = calculated_tag_table.query(
            KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(decimal.Decimal(tag_id))
                                   & Key(Constants.TIME_STAMP).begins_with(day),
            ScanIndexForward=False,
            Limit=1
        )
        calculated_tag_data = raw_data['Items'][0]
        return decimal.Decimal(calculated_tag_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
    except IndexError as err:
        logger.error('%s - Calculated Water Tags - No Data Found for tag id %s and date %s', uce, tag_id, day)
        raise WaterAnalyticsException('Calculated Water Tags - No Data Found for ' + tag_id + ' and ' + day)


def get_volume_supplied_tag(resource, plant_id, water_type, asset_type):
    """
    This function is used for getting the volume supplied per day tags for calculations
    from OperaWaterAsset Table.
    :param resource: The AWS service, we are using here DynamoDB
    :param plant_id: The plant id
    :param water_type: the water type is like raw water or clear water
    :param asset_type: the asset type like Pipe, Tank, etc.
    :return: volume of water supplied per day tag
    """
    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    source_assets = asset_table.query(
        KeyConditionExpression=Key('id').eq(plant_id),
        FilterExpression=Attr('document.parentId').contains(water_type) & Attr('document.attributes.assetType').eq(
            asset_type)
    )
    print('Source Count : ', source_assets['Count'])
    if source_assets['Count'] == 1:
        source = source_assets['Items'][0]
        parameters = source[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        logger.info('%s - Source Data Found for plant %s ', uce, plant_id)
        return parameters[Constants.VOLUME_SUPPLIED_PER_DAY]
    else:
        logger.debug('%s - Multiple Header lines found for plant : %s', uce, plant_id)
        logger.warning('%s - Multiple Header lines found for plant : %s', uce, plant_id)
        raise WaterAnalyticsException('Multiple Header Line Found for plant : ' + plant_id)


def get_energy_consumption_tags(resource, region_id, plant_id):
    """
    This function get the details of energy consumptions tags used for calculation of a plant from
    OperaWaterPlant Table.
    e.g. energy_consumption_per_day, energy_consumption_per_mld
    :param resource: the AWS service, here we are using the dynamoDB
    :param region_id: the region id
    :param plant_id:  the plant id
    :return: energy_consumption_per_day, energy_consumption_per_mld
    """
    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    try:
        plant_details = plant_table.get_item(Key={'id': region_id, 'plantId': plant_id})
        plant_item = plant_details['Item']
        plant_parameters = plant_item[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        energy_consumption_per_day = plant_parameters[Constants.ENERGY_CONSUMPTION_PER_DAY]
        energy_consumption_per_mld = plant_parameters[Constants.ENERGY_CONSUMPTION_PER_MLD]

        return energy_consumption_per_day, energy_consumption_per_mld
    except KeyError as err:
        logger.error("%s : %s", uce, err)
        raise WaterAnalyticsException('Plant Details Not Found : ' + plant_id)


def calculate_energy_consumption_for_water_supply(plant_type, asset_type):
    """
    This function is used for calculating energy consumption of a plant on daily basis.
    The Calculation id is K08 - tag id 1002079
    :param plant_type: plant type can be RWPH or CWPH
    :param asset_type: asset type can be Pump House, etc.
    :return:
    """
    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    yesterday_date = str_yesterday + " 00:00:00"

    resource = AWSService(Services.DYNAMO_DB)
    # Getting Plant details and retrieve plant ids based on the region id, plant_type and asset_tye
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, plant_type, asset_type)
    logger.info("%s - Plant ID's : %s", uce, plant_ids)
    try:
        plant_id = plant_ids[0]
        energy_per_day, energy_per_mld = get_energy_consumption_tags(resource, Region.R1, plant_id)
        volume_per_day = get_volume_supplied_tag(resource, plant_id, plant_type, 'Pipe')

        volume = get_calculated_tag_value(resource, volume_per_day[Constants.TAG_ID_KEY], str_yesterday)
        energy = get_calculated_tag_value(resource, energy_per_day[Constants.TAG_ID_KEY], str_yesterday)

        print('Volume : ', volume)
        print('Energy : ', energy)
        calculated_energy_per_mld = energy / volume
        print(calculated_energy_per_mld)

        calculated_tag_obj = water_utilities.create_calculated_object(
            energy_per_mld[Constants.TAG_ID_KEY], round(calculated_energy_per_mld, 2), yesterday_date)
        water_utilities.save_calculated_water_tag(resource, calculated_tag_obj)
    except WaterAnalyticsException as err:
        logger.error('%s - Error Occurred While Calculating Energy Consumption Per Day : %s', uce, err)


def energy_consumption_starter():
    calculate_energy_consumption_for_water_supply('RWPH', 'Pump House')


if __name__ == '__main__':
    energy_consumption_starter()