import sys
from os import path
# sys.path.append("D:\MB\Final Version\lnt-water-analytics\\")
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *
from datetime import datetime, timedelta
import decimal
import logging
from boto3.dynamodb.conditions import Key, Attr

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def is_filter_in_operation(resource, filter_in_operation, day):
    try:
        filter_data = water_utilities.get_calculated_opera_water_data(resource, decimal.Decimal(filter_in_operation[Constants.TAG_ID_KEY]), day)
        tag_value = filter_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
        if tag_value == '1' or tag_value == 1:
            return True
        else:
            return False
    except WaterAnalyticsException as err:
        logger.error('FILTER RUNNING HOURS - Filter Operation Data is not Available - %s', err)
        return False


def calculate_wtp_filter_running_hours(plant_type, asset_type, source_asset_type):
    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    resource = AWSService(Services.DYNAMO_DB)
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, plant_type, asset_type)
    print(" Plant IDs : ",  plant_ids)

    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    for plant_key in plant_ids:
        water_filters = asset_table.query(
            KeyConditionExpression=Key('id').eq(plant_key),
            FilterExpression=Attr('document.parentId').contains(plant_type) & Attr('document.attributes.assetType').eq(source_asset_type)
        )
        logger.info("Count of Items : %s", water_filters['Count'])
        for wtp_filter in water_filters['Items']:
            try:
                parameters = wtp_filter[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
                filter_in_operation = parameters[Constants.FILTER_IN_OPERATION]
                if is_filter_in_operation(resource, filter_in_operation, str_yesterday):
                    logger.info('Filter is Running : %s', wtp_filter[Constants.ASSET_ID_KEY])
                    running_hours = parameters[Constants.WTP_NUMBER_OF_RUNNING_HOURS_PER_DAY]
                    logger.info('FILTER RUNNING HOURS - getting data for ')
                    hours_data = water_utilities.get_raw_opera_water_data(
                        resource, decimal.Decimal(running_hours[Constants.TAG_ID_KEY]), str_yesterday)
                    print('Hours Data : ', hours_data)
                    value = hours_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
                    calculated_object = water_utilities.create_calculated_object(
                        running_hours[Constants.TAG_ID_KEY], value, str_yesterday + "00:00:00")
                    print('Calculated Object : ', calculated_object)
                    water_utilities.save_calculated_water_tag(resource, calculated_object)
                else:
                    logger.info('FILTER RUNNING HOURS - Filter is Not Running : %s', wtp_filter[Constants.ASSET_ID_KEY])
            except WaterAnalyticsException as err:
                logger.error('FILTER RUNNING HOURS - %s', err)
            except Exception as err:
                logger.error('FILTER RUNNING HOURS - %s', err)


if __name__ == '__main__':
    if __package__ is None:
        print("Package is none")
    calculate_wtp_filter_running_hours('WTP', 'Treatment', 'Filter')