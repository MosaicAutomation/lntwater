import sys
from os import path
# sys.path.append("D:\MB\Final Version\lnt-water-analytics\\")
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *
from datetime import datetime, timedelta
import decimal
import logging
from boto3.dynamodb.conditions import Key, Attr

logger_file = water_utilities.get_logger_file_name(Constants.CWPH_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_raw_water_data_and_calculate_volume(resource, water_intake_obj, water_volume_obj, d):
    """
    This function get raw water data from OperaWaterTags table and does calculation for OperaWaterTag Water availability
    :param resource:
    :param water_intake_obj:
    :param water_volume_obj:
    :param d:
    :return calculated_tag_object:
    """
    today_date = datetime.now().strftime("%Y-%m-%d")

    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    today_date = today_date + " 00:00:00"
    # Getting Raw Data from OperaWaterTag table
    water_tag_table = resource.get_table(Tables.OPERA_WATER_TAGS)
    # Getting Recent Data based on yesterday date
    water_intake_tagid = decimal.Decimal(water_intake_obj[Constants.TAG_ID_KEY])
    try:
        raw_data = water_tag_table.query(
            KeyConditionExpression=Key('tagId').eq(water_intake_tagid) & Key('timestamp').begins_with(str_yesterday),
            ScanIndexForward=False,
            Limit=1
        )
        print('RAW DATA : ', raw_data)
        water_data = raw_data['Items'][0]
        logger.info('K08 - Raw Water Data Found for Water Intake id %s and date %s - %s',
                    water_intake_tagid, str_yesterday, water_data)

        h = decimal.Decimal(water_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
        r = decimal.Decimal(decimal.Decimal(d) / 2)
        print('Tag Value : ', h)
        print('R  : ', r)

        volume = Constants.PI * r * r * h
        volume = round(volume, 2)
        print("Volume : ", volume)
        tag_yesterday = str_yesterday + " 00:00:00"

        calculated_document = dict()
        calculated_document[Constants.TAG_VALUE_KEY] = str(volume)
        calculated_document[Constants.TAG_ID_KEY] = decimal.Decimal(water_volume_obj[Constants.TAG_ID_KEY])
        calculated_document[Constants.TIME_STAMP] = tag_yesterday

        calculated_tag_obj = {
            Constants.TAG_ID_KEY: decimal.Decimal(water_volume_obj[Constants.TAG_ID_KEY]),
            Constants.TIME_STAMP: tag_yesterday,
            Constants.DOCUMENT_KEY: calculated_document
        }
        print('Calculated Tag Object : ', calculated_tag_obj)
        return calculated_tag_obj
    except IndexError as err:
        logger.error("K08 - Raw Water Data is Not Found for %s", str(water_intake_tagid))
        raise WaterAnalyticsException("K08 - Raw Water Data is Not Found for " + str(water_intake_tagid))


def get_water_availability(plant_type, asset_type, source_asset_type):
    """
    This functionality is for calculating daily available water in the RWPH.
    The calculation id K08 and tag id is 1002004

    :param plant_type: Raw Water or Clear Water
    :param asset_type: Pump house, etc.
    :return: None
    """
    resource = AWSService(Services.DYNAMO_DB)
    # Getting Plant details and retrieve plant ids based on the region id
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, plant_type, asset_type)
    logger.info("K08 - Plant ID's : %s ", plant_ids)

    # Search for a Source, For that WaterAsset table is used.
    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    for key in plant_ids:
        logger.info("K08 - Getting data for Plant ID : %s", key)

        try:
            source_assets = asset_table.query(
                KeyConditionExpression=Key('id').eq(key),
                FilterExpression=Attr('document.attributes.assetType').eq(source_asset_type)
                & Attr('document.assetId').contains('SRC01'),
            )
            print("============================================================")
            print('For Plant - ', key, 'Source Count : ', source_assets['Count'])
            for source in source_assets['Items']:
                try:
                    print('Source : ', source[Constants.ASSET_ID_KEY])
                    parameters = source[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
                    attributes = source[Constants.DOCUMENT_KEY][Constants.ATTRIBUTES]
                    water_intake_obj = parameters[Constants.WATER_LEVEL_AT_INTAKE]
                    water_volume_obj = parameters[Constants.VOLUME_AVAILABLE]
                    diameter = attributes[Constants.DIAMETER]
                    logger.info("K08 - Getting Details of water intake for source %s with tag id %s",
                                source[Constants.ASSET_ID_KEY], water_intake_obj[Constants.TAG_ID_KEY])
                    volume_object = get_raw_water_data_and_calculate_volume(resource, water_intake_obj, water_volume_obj, diameter)
                    logger.info('K08 - Volume Water Available : %s', volume_object)
                    water_utilities.save_calculated_water_tag(resource, volume_object)
                except WaterAnalyticsException as err:
                    logger.error(err)
        except Exception as err:
            print(err)


if __name__ == '__main__':
    get_water_availability('CWPH', 'Pump House', 'Tank')
