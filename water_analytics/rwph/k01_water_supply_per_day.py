import sys
from datetime import datetime, timedelta
import logging
# sys.path.append("D:\MB\Final Version\lnt-water-analytics\\")
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.RWPH_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_volume_supplied_data(resource, plant_id, water_type, asset_type):
    """
    This function is used for getting the volume supplied per day tags for calculations
    from OperaWaterAsset Table.
    :param resource: The AWS service, we are using here DynamoDB
    :param plant_id: The plant id
    :param water_type: the water type is like raw water or clear water
    :param asset_type: the asset type like Pipe, Tank, etc.
    :return: volume of water supplied per day tag
    """
    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    yesterday_date = str_yesterday + " 00:00:00"

    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    header_lines = asset_table.query(
        KeyConditionExpression=Key('id').eq(plant_id),
        FilterExpression=Attr('document.parentId').contains(water_type) & Attr('document.attributes.assetType').eq(
            asset_type)
    )
    if header_lines['Count'] > 0:
        for source in header_lines['Items']:
            parameters = source[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
            logger.info('K01 - Source Data Found for plant %s ', plant_id)
            flow_totalizer_tag = parameters[Constants.FLOW_TOTALIZER][Constants.TAG_ID_KEY]
            flow_data = water_utilities.get_raw_opera_water_data(resource, decimal.Decimal(flow_totalizer_tag), str_yesterday)
            logger.info('K01 - Flow_data : %s', flow_data)
            flow_volume = flow_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
            volume_per_day = parameters[Constants.VOLUME_SUPPLIED_PER_DAY]

            calculated_document = {
                Constants.TAG_ID_KEY: decimal.Decimal(volume_per_day[Constants.TAG_ID_KEY]),
                Constants.TAG_VALUE_KEY: str(flow_volume),
                Constants.TIME_STAMP: yesterday_date
            }
            calculated_tag_obj = {
                Constants.TAG_ID_KEY: decimal.Decimal(volume_per_day[Constants.TAG_ID_KEY]),
                Constants.TIME_STAMP: yesterday_date,
                Constants.DOCUMENT_KEY: calculated_document
            }
            return calculated_tag_obj
    else:
        logger.debug('K01 - Multiple Header lines found for plant : %s', plant_id)
        raise WaterAnalyticsException('K01 - Multiple Header Line Found for plant : ' + plant_id)


def get_daily_water_supplied_volume(plant_type, asset_type):
    """
    This function calculate daily volume of water supplied, it checks the recent data of particular day.
    KPI Calculation id K01 and tag id 1002001
    :param plant_type:
    :param asset_type:
    :return: None
    """
    resource = AWSService(Services.DYNAMO_DB)
    # Getting Plant details and retrieve plant ids based on the region id, plant_type and asset_type
    try:
        plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
        plants = plant_table.query(
            KeyConditionExpression=Key('id').eq(Region.R1),
            FilterExpression=Attr('document.plantId').contains(plant_type) & Attr('document.attributes.assetType').contains(
                asset_type)
        )
        for plant in plants['Items']:
            calculated_tag_obj = get_volume_supplied_data(resource, plant[Constants.PLANT_ID_KEY], plant_type, 'Pipe')
            logger.info('K01 - Calculated Object : %s', calculated_tag_obj)
            water_utilities.save_calculated_water_tag(resource, calculated_tag_obj)
            logger.info('K01 - Calculated Object Saved')

    except WaterAnalyticsException as err:
        logger.error(err)
    except Exception as err:
        logger.error(err)


if __name__ == '__main__':
    get_daily_water_supplied_volume('RWPH', 'Pump House')
    # get_daily_water_supplied_volume('CWPH', 'Pump House')