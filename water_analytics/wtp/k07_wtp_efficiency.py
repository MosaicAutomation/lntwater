import sys
from datetime import datetime, timedelta
import decimal
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_data_from_rwph(resource, day):
    """
    This function returns a K01 data from RWPH.
    :param resource:
    :return k01_value:
    """
    rwph01 = "WET:IN:RAJ:BSL:74MLD:RWPH01"
    header1 = "WET:IN:RAJ:BSL:74MLD:RWPH01:HDRL01"
    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    data = asset_table.get_item(
        Key={'id': rwph01, 'assetId': header1}
    )
    try:
        parameters = data['Item'][Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        k01_tag_id = parameters[Constants.VOLUME_SUPPLIED_PER_DAY][Constants.TAG_ID_KEY]
        print('K01 Tag Id : ', k01_tag_id)
        k01_data = water_utilities.get_calculated_opera_water_data(resource, decimal.Decimal(k01_tag_id), day)
        print(k01_data)
        return k01_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
    except WaterAnalyticsException as err:
        logger.error("WTP - PLAT EFFICIENCY - RWPH WAE %s", err)
        return 0
    except Exception as err:
        logger.error("WTP - PLAT EFFICIENCY - RWPH Exception %s ", err)
        return 0


def wtp_calculate_wtp_efficiency(plant_type, asset_type, source_asset_type):
    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")
    yesterday_date = str_yesterday + " 00:00:00"
    resource = AWSService(Services.DYNAMO_DB)

    k01_rwph_value = get_data_from_rwph(resource, str_yesterday)

    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    wtp_plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(Region.R1),
        FilterExpression=Attr('document.plantId').contains(plant_type) & Attr('document.attributes.assetType').contains(asset_type)
    )
    for plant in wtp_plants['Items']:
        parameters = plant[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        k07_tag = parameters[Constants.WTP_PLANT_EFFICIENCY][Constants.TAG_ID_KEY]
        k01_wtp_tag = parameters[Constants.WTP_K01_WATER_PROCESSED_PER_DAY][Constants.TAG_ID_KEY]
        try:
            k01_data = water_utilities.get_calculated_opera_water_data(resource, decimal.Decimal(k01_wtp_tag), str_yesterday)
            k01_wtp_value = decimal.Decimal(k01_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
            final_value = (k01_wtp_value / decimal.Decimal(k01_rwph_value)) * 100
            final_object = water_utilities.create_calculated_object(k07_tag, round(final_value, 2), yesterday_date)
            print('Final Object : ', final_object)
            water_utilities.save_calculated_water_tag(resource, final_object)
            logger.info("WTP - PLAT EFFICIENCY - Calculated Object Saved %s", final_object)
        except WaterAnalyticsException as err:
            logger.error("WTP - PLAT EFFICIENCY - WTP %s", err)
        except Exception as err:
            logger.error("WTP - PLAT EFFICIENCY - WTP %s", err)


if __name__ == '__main__':
    wtp_calculate_wtp_efficiency('WTP', 'Treatment Plant', 'Filter')