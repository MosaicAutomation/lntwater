import sys
from os import path
# sys.path.append("D:\MB\Final Version\lnt-water-analytics\\")
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *
from datetime import datetime, timedelta
import decimal
import logging


logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_data_from_cwph(resource, day):
    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)

    cwph01 = asset_table.get_item(
        Key={'id': 'WET:IN:RAJ:BSL:74MLD:CWPH01', 'assetId': 'WET:IN:RAJ:BSL:74MLD:CWPH01:HDRL01'}
    )
    try:
        parameters = cwph01['Item'][Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        k01_tag_id = parameters[Constants.VOLUME_RECEIVED_PER_DAY][Constants.TAG_ID_KEY]
        print('K01 Tag Id : ', k01_tag_id)
        k01_data = water_utilities.get_raw_opera_water_data(resource, decimal.Decimal(k01_tag_id), day)
        return k01_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
    except WaterAnalyticsException as err:
        logger.error("WTP - K01 - RWPH WAE %s", err)
        return 0
    except Exception as err:
        logger.error("WTP - K01 - RWPH Exception %s ", err)
        return 0


def calculate_filtered_water_per_day():
    resource = AWSService(Services.DYNAMO_DB)
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    try:
        k01_value = get_data_from_cwph(resource, str_yesterday)
        plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)

        wtp_plant = plant_table.get_item(
            Key={'id': Region.R1, 'plantId': 'WET:IN:RAJ:BSL:74MLD:WTP01'}
        )
        parameters = wtp_plant['Item'][Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        wtp_k01 = parameters['volOfWaterProcessedPerDay']
        print('wtp_k01 : ', wtp_k01)
        str_yesterday = str_yesterday + " 00:00:00"
        final_obj = water_utilities.create_calculated_object(wtp_k01[Constants.TAG_ID_KEY], k01_value, str_yesterday)
        print('Calculated Object : ', final_obj)
        water_utilities.save_calculated_water_tag(resource, final_obj)
    except WaterAnalyticsException as err:
        print(err)


if __name__ == '__main__':
    calculate_filtered_water_per_day()

