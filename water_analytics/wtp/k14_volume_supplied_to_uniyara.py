import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def calculate_volume_of_uniyara():
    resource = AWSService(Services.DYNAMO_DB)

    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)

    cwph04 = asset_table.get_item(
        Key={'id': 'WET:IN:RAJ:BSL:74MLD:CWPH04', 'assetId': 'WET:IN:RAJ:BSL:74MLD:CWPH04:HDRL02'}
    )

    cwph04_item = cwph04['Item']
    parameters = cwph04_item[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
    cwph_k14 = parameters[Constants.FLOW_TOTALIZER]
    fq02_tag_id = cwph_k14[Constants.TAG_ID_KEY]

    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    try:
        fq02_data = water_utilities.get_raw_opera_water_data(resource, decimal.Decimal(fq02_tag_id), str_yesterday)
        fq02_value = fq02_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
        plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
        wtp_plant = plant_table.get_item(
           Key={'id': Region.R1, 'plantId': 'WET:IN:RAJ:BSL:74MLD:WTP01'}
        )
        logger.info('WTP PLANT : %s', wtp_plant)
        wtp_parameters = wtp_plant['Item'][Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        wtp_k14_tag_id = wtp_parameters[Constants.VOLUME_SUPPLIED_TO_UNIYARA_PER_DAY][Constants.TAG_ID_KEY]
        print('wtp_k14 : ', wtp_k14_tag_id)

        yesterday = str_yesterday + " 00:00:00"
        final_obj = water_utilities.create_calculated_object(wtp_k14_tag_id, fq02_value, yesterday)
        print('Calculated Object : ', final_obj)
        water_utilities.save_calculated_water_tag(resource, final_obj)
    except WaterAnalyticsException as err:
        logger.error("K14 - %s ", err)


if __name__ == '__main__':
    calculate_volume_of_uniyara()
