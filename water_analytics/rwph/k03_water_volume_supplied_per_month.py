import sys
from datetime import datetime, timedelta
import logging
# sys.path.append("D:\MB\Final Version\lnt-water-analytics\\")
from dateutil.relativedelta import relativedelta
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.RWPH_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_water_volume_supplied_for_month(resource, tag_id):
    """
    This function get the last month energy consumption data for all days and add all day's values
    and return aggregated monthly energy consumptions
    :param resource:
    :param tag_id:
    :return monthly_energy_consumption:
    """

    last_month_day = datetime.today() - relativedelta(months=1)
    last_month_yyyy_mm = last_month_day.strftime('%Y-%m')
    water_tag_table = resource.get_table(Tables.OPERA_WATER_CALCULATED_TAGS)
    raw_data = water_tag_table.query(
        KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(tag_id) & Key(Constants.TIME_STAMP).begins_with(last_month_yyyy_mm),
    )
    logger.info('K03 - Last Month Energy Consumption Data : %s', raw_data)
    monthly_energy_consumption = 0
    for raw in raw_data['Items']:
        monthly_energy_consumption += decimal.Decimal(raw[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
    logger.debug('K03 - Calculated Monthly Consumption Value : %s', monthly_energy_consumption)

    return monthly_energy_consumption


def calculate_monthly_volume_of_water_supplied(plant_type, asset_type, source_asset_type):
    """
    This function calculates monthly energy consumptions by adding daily energy consumptions.
    Calculation KPI id : K03 and Tag Id : 1002003
    :param plant_type:
    :param asset_type:
    :return:
    """
    today = datetime.today()
    first_day_of_month = date(today.year, today.month, 1)
    first_day_of_month_timestamp = first_day_of_month.strftime('%Y-%m-%d') + " 00:00:00"
    resource = AWSService(Services.DYNAMO_DB)
    # Getting Plant details and retrieve plant ids based on the region id, plant_type and asset_tye
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, plant_type, asset_type)
    logger.info("K03 - Plant ID's : %s", plant_ids)
    for plant in plant_ids:
        try:
            asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
            query_data = asset_table.query(
                KeyConditionExpression=Key('id').eq(plant),
                FilterExpression=Attr('document.parentId').contains(plant_type) & Attr(
                    'document.attributes.assetType').eq(source_asset_type)
            )
            print('Source Assets : ', query_data)
            if query_data['Items']:
                for source in query_data['Items']:
                    parameters = source[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
                    volume_per_day = parameters[Constants.VOLUME_SUPPLIED_PER_DAY]
                    volume_per_month = parameters[Constants.VOLUME_SUPPLIED_PER_MONTH]
                    monthly_volume_value = get_water_volume_supplied_for_month(resource,
                                                decimal.Decimal(volume_per_day[Constants.TAG_ID_KEY]))

                    monthly_volume_object = water_utilities.create_calculated_object(
                        volume_per_month[Constants.TAG_ID_KEY], round(monthly_volume_value, 2), first_day_of_month_timestamp)
                    print('Monthly Volume Object : ', monthly_volume_object)
                    water_utilities.save_calculated_water_tag(resource, monthly_volume_object)
            else:
                logger.debug("No Data Found for Asset Table for %s plant ", plant)
                raise WaterAnalyticsException("No Data Found for Asset Table for plant " + plant)
        except WaterAnalyticsException as err:
            logger.error(err)
        except Exception as err:
            logger.error(err)


if __name__ == '__main__':
    calculate_monthly_volume_of_water_supplied('RWPH', 'Pump House', 'Pipe')