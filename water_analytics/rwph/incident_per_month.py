import sys
from datetime import datetime, timedelta
import logging
from dateutil.relativedelta import relativedelta
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.INCIDENT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_last_month_incident_count(resource, plant_type, month):
    water_tag_table = resource.get_table(Tables.OPERA_WATER_INCIDENT)
    raw_data = water_tag_table.query(
        KeyConditionExpression=Key(Constants.PLANT_ID_KEY).eq(plant_type) & Key('timeStamp').begins_with(month),
    )
    if raw_data['Items']:
        return len(raw_data['Items'])
    else:
        logger.info('ALARM - No alarm for %s and %s ', plant_type, month)


def get_incident_per_month_for_each_plant():
    resource = AWSService(Services.DYNAMO_DB)
    last_month_day = datetime.today() - relativedelta(months=1)
    last_month_yyyy_mm = last_month_day.strftime('%Y-%m')

    today = datetime.today()
    first_day_of_month = date(today.year, today.month, 1)
    first_day_of_month_timestamp = first_day_of_month.strftime('%Y-%m-%d') + " 00:00:00"

    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(Region.R1)
    )
    for plant in plants['Items']:
        try:
            logger.info('Getting Incident details for Plant %s ', plant[Constants.PLANT_ID_KEY])
            tag_id = plant[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY][Constants.INCIDENT_PER_MONTH]
            value = get_last_month_incident_count(resource, plant[Constants.PLANT_ID_KEY], last_month_yyyy_mm)
            if not value:
                value = 0
            final_object = water_utilities.create_calculated_object(tag_id[Constants.TAG_ID_KEY], str(value), first_day_of_month_timestamp)
            water_utilities.save_calculated_water_tag(resource, final_object)
        except KeyError as err:
            print('KeyError : ', err)


if __name__ == '__main__':
    get_incident_per_month_for_each_plant()

