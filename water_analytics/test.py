import sys
from os import path
sys.path.append("D:\MB\Final Version\lnt-water-analytics\\")
from water_analytics.utilities import *

from datetime import datetime, timedelta
import decimal
import logging
from boto3.dynamodb.conditions import Key, Attr


def create_data():
    resource = AWSService(Services.DYNAMO_DB)
    water_tag_table = resource.get_table(Tables.OPERA_WATER_INCIDENT)
    search_dt = "2018-02-04"
    raw_data = water_tag_table.scan()

    dt = "2018-02-04"
    for raw in raw_data['Items']:
        # print('RAW : ', raw)
        ti = raw['timeStamp'].split(' ')

        raw['timeStamp'] = dt + " " + ti[1]
        print('RAW : ', raw['timeStamp'])
        save_calculated_incident(resource, raw)


if __name__ == '__main__':
    create_data()