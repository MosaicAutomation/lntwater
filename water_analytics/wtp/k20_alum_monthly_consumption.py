import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *
from dateutil.relativedelta import relativedelta

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def calculate_wtp_k20(plant_type, asset_type):
    """
    This function calculate the Unit consumption of Alum for supplying MLD of water.

    :param plant_type:
    :param asset_type:
    :return:
    """
    today = datetime.today()
    first_day_of_month = date(today.year, today.month, 1)
    first_day_of_month_timestamp = first_day_of_month.strftime('%Y-%m-%d') + " 00:00:00"

    resource = AWSService(Services.DYNAMO_DB)
    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(Region.R1),
        FilterExpression=Attr('document.plantId').contains(plant_type) & Attr('document.attributes.assetType').contains(
            asset_type)
    )
    for plant in plants['Items']:
        try:
            parameters = plant[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
            alum_tag_key = decimal.Decimal(parameters[Constants.WTP_K17_UNIT_ALUM_CONSUMPTION][Constants.TAG_ID_KEY])
            k20 = parameters[Constants.WTP_K20_MONTHLY_ALUM_CONSUMPTION][Constants.TAG_ID_KEY]
            last_month_day = datetime.today() - relativedelta(months=1)
            last_month_yyyy_mm = last_month_day.strftime('%Y-%m')
            water_tag_table = resource.get_table(Tables.OPERA_WATER_CALCULATED_TAGS)
            print('Last Month : ', last_month_yyyy_mm, alum_tag_key)
            raw_data = water_tag_table.query(
                KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(alum_tag_key) &
                Key(Constants.TIME_STAMP).begins_with(last_month_yyyy_mm),
            )
            monthly_alum_consumption = 0
            print('RAW DATA ', raw_data)
            for raw in raw_data['Items']:
                monthly_alum_consumption += decimal.Decimal(raw[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
            print('monthly_alum_consumption : ', monthly_alum_consumption)
            final_obj = water_utilities.create_calculated_object(k20, round(monthly_alum_consumption, 3), first_day_of_month_timestamp)
            print("Final Object : ", final_obj)
            water_utilities.save_calculated_water_tag(resource, final_obj)

        except WaterAnalyticsException as err:
            logger.error(err)
        except Exception as err:
            logger.error(err)


if __name__ == '__main__':
    calculate_wtp_k20('WTP', 'Treatment Plant')
