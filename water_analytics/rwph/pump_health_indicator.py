import sys
from datetime import datetime, timedelta
import logging
import botocore
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *


logger_file = water_utilities.get_logger_file_name(Constants.RWPH_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_bearing_temperature_label(temperature):
    print('In Bearing Temperature : ', temperature)
    if 60 < temperature <= 80:
        return ColorLabel.GREEN, ColorLabel.GREEN_VALUE
    elif 80 < temperature <= 90:
        return ColorLabel.YELLOW, ColorLabel.YELLOW_VALUE
    elif 90 < temperature <= 100:
        return ColorLabel.ORANGE, ColorLabel.ORANGE_VALUE
    elif temperature > 100:
        return ColorLabel.RED, ColorLabel.RED_VALUE
    # else:
    #     return 0, 0


def get_pump_vibration_label(vibrate_rate):
    if vibrate_rate > 7:
        return ColorLabel.RED, ColorLabel.RED_VALUE
    elif vibrate_rate > 5:
        return ColorLabel.ORANGE, ColorLabel.ORANGE_VALUE
    elif vibrate_rate > 2.4:
        return ColorLabel.YELLOW, ColorLabel.YELLOW_VALUE
    else:
        return ColorLabel.GREEN, ColorLabel.GREEN_VALUE


def check_health_status(*args):
    if all(temp == ColorLabel.GREEN_VALUE for temp in args):
        return ColorLabel.GREEN_VALUE
    elif any(temp == ColorLabel.YELLOW_VALUE for temp in args):
        return ColorLabel.YELLOW_VALUE
    elif any(temp == ColorLabel.ORANGE_VALUE for temp in args):
        return ColorLabel.ORANGE_VALUE
    elif any(temp == ColorLabel.RED_VALUE for temp in args):
        return ColorLabel.RED_VALUE


def get_raw_water_data_tag_value(resource, tag_id):
    today_date = datetime.now().strftime("%Y-%m-%d")
    # Getting Raw Data from OperaWaterTag table
    water_tag_table = resource.get_table(Tables.OPERA_WATER_TAGS)
    try:
        raw_data = water_tag_table.query(
            KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(decimal.Decimal(tag_id))
            & Key(Constants.TIME_STAMP).begins_with(today_date),
            ScanIndexForward=False,
            Limit=1
        )
        print('RAW DATA : ', raw_data)
        pump_bt_data = raw_data['Items'][0]
        tag_value = pump_bt_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
        return tag_value
    except IndexError as err:
        raise WaterAnalyticsException('PHI - No Data Found for the requested tag Id : ' + str(tag_id))


def get_pump_health_status(resource, pump_object):
    bt_mde = get_raw_water_data_tag_value(resource, pump_object[Constants.BT_MOTOR_DE][Constants.TAG_ID_KEY])
    logger.info('PHI - BT MDE for PUMP %s is %s ', pump_object[Constants.ASSET_ID_KEY], bt_mde)
    bt_mde_label, bt_mde_value = get_bearing_temperature_label(float(bt_mde))

    bt_mnde = get_raw_water_data_tag_value(resource, pump_object[Constants.BT_MOTOR_NDE][Constants.TAG_ID_KEY])
    logger.info('PHI - BT MNDE for PUMP %s is %s ', pump_object[Constants.ASSET_ID_KEY], bt_mnde)
    bt_mnde_label, bt_mnde_value = get_bearing_temperature_label(float(bt_mnde))

    bt_pump = get_raw_water_data_tag_value(resource, pump_object[Constants.BT_PUMP][Constants.TAG_ID_KEY])
    logger.info('PHI - BT PUMP for PUMP %s is %s ', pump_object[Constants.ASSET_ID_KEY], bt_pump)
    bt_pump_label, bt_pump_value = get_bearing_temperature_label(float(bt_pump))

    pump_vibrate = get_raw_water_data_tag_value(resource, pump_object[Constants.VIBRATION_PUMP][Constants.TAG_ID_KEY])
    logger.info('PHI - Vibrate rate for PUMP %s is %s', pump_object[Constants.ASSET_ID_KEY], pump_vibrate)
    vibrate_label, vibrate_value = get_pump_vibration_label(float(pump_vibrate))

    final_pump_status = check_health_status(bt_mde_value, bt_mnde_value, bt_pump_value, vibrate_value)

    return final_pump_status


def is_pump_running(resource, pump):
    try:
        value = get_raw_water_data_tag_value(resource, pump[Constants.TAG_ID_KEY])
        if value == 1 or value == '1':
            return True
        else:
            return False
    except WaterAnalyticsException as err:
        logger.info("PHI - %s Pump is not running %s ", pump[Constants.TAG_ID_KEY], err)
        return False


def check_water_pump_health_index(water_type, asset_type):
    """
    This is hourly activity where we will check for the pump health based on the four parameters.
    1. BT DE
    2. BT NDE
    3. BT Pump
    4. Vibration
    KPI ID : P01PHI	P02PHI	P03PHI	P04PHI	P05PHI	P06PHI	P07PHI	P08PHI	P09PHI
    TAG ID :1002089	1002090	1002091	1002092	1002093	1002094	1002095	1002096	1002097

    :param water_type:
    :param asset_type:
    :return:
    """
    try:
        resource = AWSService(Services.DYNAMO_DB)
        plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, water_type, 'Pump House')
        logger.info('PHI - Getting data for plants : %s', plant_ids)
        # Search for a Source, For that WaterAsset table is used.
        asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
        asset_pump_list = []
        for plant_key in plant_ids:
            source_assets = asset_table.query(
                KeyConditionExpression=Key('id').eq(plant_key),
                FilterExpression=Attr('document.parentId').contains(water_type) & Attr('document.attributes.assetType').eq(asset_type)
            )
            for sa in source_assets['Items']:
                parameters = sa[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
                asset = {
                    Constants.ASSET_ID_KEY: sa[Constants.ASSET_ID_KEY],
                    Constants.BT_MOTOR_DE: parameters[Constants.BT_MOTOR_DE],
                    Constants.BT_MOTOR_NDE: parameters[Constants.BT_MOTOR_NDE],
                    Constants.BT_PUMP: parameters[Constants.BT_PUMP],
                    Constants.VIBRATION_PUMP: parameters[Constants.VIBRATION_PUMP],
                    Constants.PUMP_HEALTH_INDEX: parameters[Constants.PUMP_HEALTH_INDEX],
                    Constants.PUMP_RUN_FEEDBACK: parameters[Constants.PUMP_RUN_FEEDBACK]
                }
                asset_pump_list.append(asset)

            today_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            for pump in asset_pump_list:
                # Check for running pump
                try:
                    logger.info('PHI - Checking for running state of PUMP %s', pump[Constants.ASSET_ID_KEY])
                    if is_pump_running(resource, pump[Constants.PUMP_RUN_FEEDBACK]):
                        logger.info('PHI - Pump is in RUNNING state : %s', pump[Constants.ASSET_ID_KEY])
                        status = get_pump_health_status(resource, pump)
                        calculated_object = {
                            Constants.TAG_ID_KEY: decimal.Decimal(pump[Constants.PUMP_HEALTH_INDEX][Constants.TAG_ID_KEY]),
                            Constants.TIME_STAMP: today_date,
                            Constants.DOCUMENT_KEY: {
                                Constants.TAG_ID_KEY: decimal.Decimal(pump[Constants.PUMP_HEALTH_INDEX][Constants.TAG_ID_KEY]),
                                Constants.TAG_VALUE_KEY: str(status),
                                Constants.TIME_STAMP: today_date
                            }
                        }
                        # water_utilities.save_calculated_water_tag(resource, calculated_object)
                        logger.info('PHI - Calculated Object Saved for : %s', calculated_object)
                    else:
                        logger.error('PHI - Pump is NOT in RUNNING state : %s', pump[Constants.ASSET_ID_KEY])
                except WaterAnalyticsException as err:
                    logger.error(err)
    except botocore.errorfactory.ProvisionedThroughputExceededException as err:
        logger.error('PHI - AWS DYNAMO_DB ProvisionedThroughputExceededException')
        logger.error('PHI - %s ', err)


if __name__ == '__main__':
    check_water_pump_health_index('RWPH', 'Pump')