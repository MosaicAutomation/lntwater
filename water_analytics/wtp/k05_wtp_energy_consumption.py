import sys
from datetime import datetime, timedelta
import decimal
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def calculate_energy_consumption_per_day(plant_type, asset_type):
    """
    This function is used to calculate the energy consumption on daily basis.
    It has an addition of values of powerConsumptionIncomer 1 and powerConsumptionIncomer 2.
    KPI Calculation ID : K05 and Tag ID : 1002020
    :param plant_type:
    :param asset_type:
    :return:
    """
    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")
    yesterday_date = str_yesterday + " 00:00:00"

    resource = AWSService(Services.DYNAMO_DB)
    # Getting Plant details and retrieve plant ids based on the region id, plant_type and asset_tye
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, plant_type, asset_type)
    logger.info("WTP - K05 - Plant ID's : %s", plant_ids)

    for plant in plant_ids:
        try:
            plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
            query_data = plant_table.get_item(
                Key={
                    'id': Region.R1, Constants.PLANT_ID_KEY: plant
                }
            )
            if query_data['Item']:
                plant_data = query_data['Item']
                parameters = plant_data[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
                pci1 = parameters[Constants.POWER_CONSUMPTION_INCOMER1]
                pci2 = parameters[Constants.POWER_CONSUMPTION_INCOMER2]
                pc = parameters[Constants.ENERGY_CONSUMPTION_PER_DAY]
                pci1_data = water_utilities.get_raw_opera_water_data(resource,
                                                                     decimal.Decimal(pci1[Constants.TAG_ID_KEY]), str_yesterday)
                pci2_data = water_utilities.get_raw_opera_water_data(resource,
                                                                     decimal.Decimal(pci2[Constants.TAG_ID_KEY]), str_yesterday)

                pci1_value = pci1_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
                pci2_value = pci2_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
                logger.info('WTP - K05 - PCI1 : %s', pci1_data)
                logger.info('WTP - K05 - PCI2 : %s', pci2_data)
                pc_value = decimal.Decimal(pci1_value) + decimal.Decimal(pci2_value)

                pc_object = water_utilities.create_calculated_object(pc[Constants.TAG_ID_KEY], pc_value, yesterday_date)
                print('Final Object : ', pc_object)
                water_utilities.save_calculated_water_tag(resource, pc_object)
            else:
                logger.debug("WTP - K05 - No Data Found for Plant %s in Plant Table", plant)
                logger.warning("WTP - K05 - No Data Found for Plant %s in Plant Table", plant)
                raise WaterAnalyticsException("WTP - No Data Found for Plant : " + plant)
        except WaterAnalyticsException as err:
            logger.error(err)


if __name__ == '__main__':
    calculate_energy_consumption_per_day('WTP', 'Treatment Plant')
