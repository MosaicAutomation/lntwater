from .constants import Tables
from .constants import Constants
from boto3.dynamodb.conditions import Key, Attr
from .water_exception import WaterAnalyticsException
import decimal
from datetime import date


def get_logger_file_name(plant_type):
    # PATH = '/home/ec2-user/lnt-water-analytics/logs/'
    PATH = 'D:\\MB\\Final Version\\lnt-water-analytics\\logs\\'
    current_date = date.today().strftime('/log_%d_%m_%Y.log')
    file_name = PATH + plant_type + current_date
    return file_name


def create_calculated_object(tag_id, value, day):
    calculated_document = {
        Constants.TAG_ID_KEY: decimal.Decimal(tag_id),
        Constants.TAG_VALUE_KEY: str(value),
        Constants.TIME_STAMP: day
    }
    calculated_tag_obj = {
        Constants.TAG_ID_KEY: decimal.Decimal(tag_id),
        Constants.TIME_STAMP: day,
        Constants.DOCUMENT_KEY: calculated_document
    }
    return calculated_tag_obj


def get_raw_opera_water_data(resource, tag_id, day):
    try:
        water_tag_table = resource.get_table(Tables.OPERA_WATER_TAGS)
        raw_data = water_tag_table.query(
            KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(tag_id) & Key(Constants.TIME_STAMP).begins_with(day),
            ScanIndexForward=False,
            Limit=1
        )
        raw_water_data = raw_data['Items'][0]
        return raw_water_data
    except IndexError as err:
        raise WaterAnalyticsException('No Data found for tag id ' + str(tag_id) + ' and for date ' + day)


def get_calculated_opera_water_data(resource, tag_id, day):
    try:
        water_tag_table = resource.get_table(Tables.OPERA_WATER_CALCULATED_TAGS)
        raw_data = water_tag_table.query(
            KeyConditionExpression=Key('tagId').eq(tag_id) & Key('timestamp').begins_with(day),
            ScanIndexForward=False,
            Limit=1
        )
        raw_water_data = raw_data['Items'][0]
        return raw_water_data
    except IndexError as err:
        raise WaterAnalyticsException('No Data found for tag id ' + str(tag_id) + ' and date ' + day + ' - ')


def get_plant_ids_of_region(resource, region, plant_type, asset_type):
    """
    This function returns the plant ids based on the region, plant type and asset types.
    :param resource:
    :param region:
    :param plant_type:
    :param asset_type:
    :return: list
    """
    # Getting Plant details and retrieve plant ids based on the region id
    # Explicitely for pump houses
    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(region),
        FilterExpression=Attr('document.plantId').contains(plant_type) & Attr('document.attributes.assetType').contains(asset_type)
    )
    plant_ids = [plant['plantId'] for plant in plants['Items']]
    return plant_ids


def save_calculated_water_tag(resource, calculated_object):
    """
    This function saves the calculated object to OperaWaterCalculatedTags table
    :param resource:
    :param calculated_object:
    :return:
    """
    calculated_tag_table = resource.get_table(Tables.OPERA_WATER_CALCULATED_TAGS)
    calculated_tag_table.put_item(Item=calculated_object)


def save_calculated_incident(resource, incident_object):
    calculated_tag_table = resource.get_table(Tables.OPERA_WATER_INCIDENT)
    calculated_tag_table.put_item(Item=incident_object)


def save_calculated_tag_in_current_tag(resource, obj):
    calculated_tag_table = resource.get_table(Tables.OPERA_WATER_CURRENT_TAG)
    calculated_tag_table.put_item(Item=obj)