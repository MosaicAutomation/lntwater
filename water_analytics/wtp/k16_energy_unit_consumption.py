import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def calculate_wtp_k16(plant_type, asset_type):
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    yesterday_date = str_yesterday + " 00:00:00"
    resource = AWSService(Services.DYNAMO_DB)

    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(Region.R1),
        FilterExpression=Attr('document.plantId').contains(plant_type) & Attr('document.attributes.assetType').contains(
            asset_type)
    )
    for plant in plants['Items']:
        try:
            parameters = plant[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
            k01 = parameters[Constants.WTP_K01_WATER_PROCESSED_PER_DAY][Constants.TAG_ID_KEY]
            k05 = parameters[Constants.WTP_K05_ENERGY_CONSUMPTION_PER_DAY][Constants.TAG_ID_KEY]
            k01_data = water_utilities.get_calculated_opera_water_data(resource, decimal.Decimal(k01), str_yesterday)
            k01_value = decimal.Decimal(k01_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])

            try:
                k16 = parameters[Constants.WTP_K16_UNIT_ENERGY_CONSUMPTION][Constants.TAG_ID_KEY]
                k05_data = water_utilities.get_calculated_opera_water_data(resource, decimal.Decimal(k05), str_yesterday)
                k05_value = decimal.Decimal(k05_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
                logger.info('Value of %s is %s and Value of %s is %s', k01, k01_value, k05, k05_value)
                k16_value = k05_value / k01_value
                final_obj = water_utilities.create_calculated_object(k16, round(k16_value, 3), yesterday_date)
                print("Final Object : ", final_obj)
                water_utilities.save_calculated_water_tag(resource, final_obj)
            except WaterAnalyticsException as err:
                logger.error('K16 - Error Occurred in K16 Calculations : %s', err)
        except WaterAnalyticsException as err:
            logger.error(err)
        except Exception as err:
            logger.error(err)


if __name__ == '__main__':
    calculate_wtp_k16('WTP', 'Treatment Plant')
