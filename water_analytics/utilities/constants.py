import decimal


class Constants:
    WTP_PLANT = 'wtp'
    RWPH_PLANT = 'rwph'
    CWPH_PLANT = 'cwph'
    INCIDENT = 'incident'

    PI = decimal.Decimal(3.142)
    ATTRIBUTES = 'attributes'
    CLASSIFICATION = 'classification'
    DIAMETER = 'diameter'
    DOCUMENT_KEY = 'document'
    INNER_DIAMETER = 'innerDiameter'
    OUTER_DIAMETER = 'outerDiameter'
    TAG_ID_KEY = "tagId"
    TAG_VALUE_KEY = 'tagValue'
    ASSET_ID_KEY = 'assetId'
    PLANT_ID_KEY = 'plantId'
    VOLUME_KEY = 'volume'
    TIME_STAMP = 'timestamp'
    WATER_LEVEL_AT_INTAKE = "waterLevelAtIntake"
    VOLUME_AVAILABLE = "volumeAvailable"
    ASSET_TYPE = "Tank"
    LOOKUP_TABLE_FOR_VOLUME_CALC = 'lookupTableForVolumeCalc'
    POWER_CONSUMPTION_KEY = 'powerConsumption'
    PARAMETERS_KEY = 'parameters'
    PUMP_POWER_CONSUMPTION_PER_DAY_KEY = 'pumpPowerConsumptionPerDay'
    BT_MOTOR_DE = 'bearingTemperatureMotorDE'
    BT_MOTOR_NDE = 'bearingTemperatureMotorNDE'
    BT_PUMP = 'bearingTemperaturePump'
    VIBRATION_PUMP = 'vibration'
    PUMP_HEALTH_INDEX = 'pumpHealthIndex'
    PUMP_RUN_FEEDBACK = 'pumpRunFeedback'
    ENERGY_CONSUMPTION_PER_DAY = 'energyConsumptionPerDay'
    ENERGY_CONSUMPTION_PER_MLD = 'energyConsumptionPerMlD'
    VOLUME_SUPPLIED_PER_DAY = 'volumeSuppliedPerDay'
    FLOW_TOTALIZER = 'flowTotalizer'
    PUMP_RUNNING_HOURS_PER_DAY = 'pumpRunningHoursPerDay'
    RUNNING_HOURS = 'runningHours'
    NUMBER_OF_RUNNING_HOURS_PER_DAY = 'numberOfRunningHrsPerDay'
    POWER_CONSUMPTION_INCOMER1 = 'powerConsumptionIncomer1'
    POWER_CONSUMPTION_INCOMER2 = 'powerConsumptionIncomer2'
    VOLUME_SUPPLIED_PER_MONTH = 'volumeSuppliedPerMonth'
    ENERGY_CONSUMPTION_PER_MONTH = 'energyConsumptionPerMonth'
    # LOG_FILE_PATH = '/home/ec2-user/lnt-water-analytics/logs/'
    LOG_FILE_PATH = 'logs/'
    ASSET_NAME = 'assetName'
    REGION_ID_KEY = 'regionId'
    VOLUME_RECEIVED_PER_DAY = 'volumeReceivedPerDay'
    VOLUME_RECEIVED_PER_MONTH = 'volumeReceivedPerMonth'
    # CWPH_ENERGY_CONSUMPTION_PER_MLD = 'energyConsumptionPerMLDOfWaterPerDay'
    INCIDENT_PER_MONTH = 'incidentsPerMonth'

    FILTER_BED_INLET_VALVE_STATUS = 'filterBedInletValveStatus'
    FILTERED_WATER_OUTLET_VALVE_STATUS = 'filteredWaterOutletValveStatus'
    BACKWASH_INLET_VALVE_STATUS = 'backwashInletValveStatus'
    AIR_SOURING_VALVE_STATUS = 'airScouringValveStatus'
    BACKWASH_OUTLET_VALVE_STATUS = 'outletValveToDirtyBackwashWaterChannelStatus'
    FILTER_IN_OPERATION = 'filterInOperation'
    FILTER_IN_DIRTY_BACKWASH = 'filterInDirtyBackwash'
    FILTER_IN_MAINTENANCE = 'filterInMaintenance'
    FILTER_IN_BACKWASH = 'filterInBackwash'
    WTP_FILTER_FLOW_RATE = 'filterFlowRate'
    WTP_FILTER_EFFICIENCY = 'filterEfficiency'
    WTP_FILTER_BED_LEVEL = 'filterBedLevel'
    WTP_NUMBER_OF_RUNNING_HOURS_PER_DAY = 'noOfRunningHoursPerDay'

    WTP_K01_WATER_PROCESSED_PER_DAY = 'volOfWaterProcessedPerDay'
    WTP_K05_ENERGY_CONSUMPTION_PER_DAY = 'energyConsumptionPerDay'
    WTP_K16_UNIT_ENERGY_CONSUMPTION = 'unitConsumptionOfEnergyForSupplyingMLDOfWater'
    WTP_K17_UNIT_ALUM_CONSUMPTION = 'unitConsumptionOfAlumForSupplyingMLDOfWater'
    WTP_K18_UNIT_LIME_CONSUMPTION = 'unitConsumptionOfLimeForSupplyingMLDOfWater'
    WTP_K19_UNIT_CHLORINE_CONSUMPTION = 'unitConsumptionOfChlorineForSupplyingMLDOfWater'
    WTP_LIME_STOCK_AVAILABILITY = 'limeStockAvailability'
    WTP_ALUM_STOCK_AVAILABILITY = 'alumStockAvailability'
    WTP_CHLORINE_STOCK_AVAILABILITY = 'chlorineStockAvailability'
    WTP_CHLORINE_CONSUMPTION = 'chlorineConsumption'
    WTP_LIME_CONSUMPTION = 'limeConsumption'
    WTP_ALUM_CONSUMPTION = 'alumConsumption'
    WTP_K20_MONTHLY_ALUM_CONSUMPTION = 'monthlyAverageAlumConsumption'
    WTP_K21_MONTHLY_LIME_CONSUMPTION = 'monthlyAverageLimeConsumption'
    WTP_K22_MONTHLY_CHLORINE_CONSUMPTION = 'monthlyAverageChlorineConsumption'
    WTP_BACKWASH_WATER_FLOW_RATE = 'backwashWaterTankFlowRate'
    WTP_BACKWASH_TANK_WATER_FLOW_TOTALIZER = 'flowTotalizerValueForBackwashTankFlow'
    WTP_NUMBER_OF_OPERATIONAL_HOURS_PER_DAY = 'numberOfOperationalHrsPerDay'
    WTP_WATER_PROCESSED_PER_DAY = 'volOfWaterProcessedPerDay'
    WTP_WATER_PROCESSED_PER_MONTH = 'volumeProcessedPerMonth'
    WTP_PLANT_EFFICIENCY = 'efficiency'
    WTP_VOLUME_USED_IN_BACKWASH_OPERATION = 'volumeUsedInBackwashOperation'
    VOLUME_SUPPLIED_TO_DEOLI_PER_DAY = 'volSuppliedtoDeoliPerDay'
    VOLUME_SUPPLIED_TO_TONK_PER_DAY = 'volSuppliedtoTonkPerDay'
    VOLUME_SUPPLIED_TO_UNIYARA_PER_DAY = 'volSuppliedtoUniyaraPerDay'
    WTP_VOLUME_AVG_TURBIDITY_BEFORE_FILTRATION = 'avgTurbidityBeforeFiltration'
    WTP_TURBIDITY_VALUE_BEFORE_FILTRATION = 'turbidityValueBeforeFiltration'
    WTP_BACKWASH_TANK_LEVEL = 'backwashTankLevel'
    WTP_VOLUME_AVAILABLE_IN_BACKWASH_TANK = 'volumeAvailableInBackwashTank'


class Tables:
    OPERA_WATER_REGION = "OperaWaterRegion"
    OPERA_WATER_PLANT = "OperaWaterPlant"
    OPERA_WATER_ASSET = "OperaWaterAsset"
    OPERA_WATER_TAGS = "OperaWaterTags"
    OPERA_WATER_LOOKUP_TABLE = "OperaWaterLookUpTable"
    OPERA_WATER_CALCULATED_TAGS = "OperaWaterCalculatedTags"
    OPERA_WATER_ALARM = 'OperaWaterAlarm'
    OPERA_WATER_INCIDENT = 'OperaWaterIncident'
    OPERA_WATER_ALARM_LOOKUP = 'AlarmTagLookUpTable'
    OPERA_WATER_CURRENT_TAG = 'OperaWaterCurrentTag'


class ColorLabel:
    GREEN = 'Green'
    RED = 'Red'
    YELLOW = 'Yellow'
    ORANGE = 'Orange'
    GREEN_VALUE = 100
    YELLOW_VALUE = 66
    ORANGE_VALUE = 33
    RED_VALUE = 0


class Services:
    DYNAMO_DB = "dynamodb"


class Region:
    R1 = "R1"


class AlarmTag:
    RWPH_ZEROTH_BIT_TAGS = ['10024', '10037', '10050', '10063', '10076', '10089', '100102', '100115', '100128', '10026',
                       '10039', '10052', '10065', '10078', '10091', '100104', '100117', '100130', '10025', '10038',
                       '10051', '10064', '10077', '10090', '100103', '100116', '100129', '1008', '10013']
    RWPH_ALL_BIT_TAGS = ['1009', '10010', '10011', '10012']

