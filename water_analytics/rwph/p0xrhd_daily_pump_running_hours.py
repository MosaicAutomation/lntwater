import sys
from datetime import datetime, timedelta
import logging
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.RWPH_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_raw_water_data_and_calculate_tag(resource, pump_data):
    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    today_date = today_date + " 00:00:00"
    # Getting Raw Data from OperaWaterTag table
    water_tag_table = resource.get_table(Tables.OPERA_WATER_TAGS)
    # Getting Recent Data based on yesterday date
    logger.info('PRHD - KWARGS : %s', pump_data)
    running_hours_key = decimal.Decimal(pump_data[Constants.RUNNING_HOURS][Constants.TAG_ID_KEY])
    try:
        raw_data = water_tag_table.query(
            KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(running_hours_key)
            & Key(Constants.TIME_STAMP).begins_with(str_yesterday),
            ScanIndexForward=False,
            Limit=1
        )
        hours_data = raw_data['Items'][0]
        logger.info('PRHD - Raw Power Data Found for power consumption id %s and date %s - %s', running_hours_key, str_yesterday, hours_data)
        tag_value = hours_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]

        tag_yesterday = str_yesterday + " 00:00:00"

        calculated_tag_obj = water_utilities.create_calculated_object(
            decimal.Decimal(pump_data[Constants.PUMP_RUNNING_HOURS_PER_DAY][Constants.TAG_ID_KEY]),
            tag_value, tag_yesterday
        )
        return calculated_tag_obj
    except IndexError as err:
        logger.error('PRHD - %s', err)
        raise WaterAnalyticsException("PRHD - Raw Power Data is Not Found for " + str(running_hours_key))


def calculate_running_hours_for_all_pumps(plant_type, asset_type):
    """
    This function calculates the daily running hours for all pumps individually.
    i.e. running hours per day for each pump
    KPI ID: P01RHD,	 P02RHD,   P03RHD,	P04RHD,	 P05RHD,  P06RHD,  P07RHD,	P08RHD, P09RHD
    TAG ID: 1002007, 1002008, 1002009, 1002010, 1002011, 1002012, 1002013, 1002014, 1002015

    :param plant_type:
    :param asset_type:
    :return: None
    """
    resource = AWSService(Services.DYNAMO_DB)
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, plant_type, 'Pump House')
    logger.info('PRHD - Getting data for plants : %s', plant_ids)
    # Search for a Source, For that WaterAsset table is used.
    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    asset_list = []
    for plant_key in plant_ids:
        source_assets = asset_table.query(
            KeyConditionExpression=Key('id').eq(plant_key),
            FilterExpression=Attr('document.parentId').contains(plant_type) & Attr('document.attributes.assetType').eq(asset_type)
        )
        logger.info('PRHD - %s source items found ', source_assets['Count'])

        for sa in source_assets['Items']:
            # assetId, runningHours, pumpRunningHoursPerDay
            parameters = sa[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
            asset = {
                Constants.ASSET_ID_KEY: sa[Constants.ASSET_ID_KEY],
                Constants.RUNNING_HOURS: parameters[Constants.RUNNING_HOURS],
                Constants.PUMP_RUNNING_HOURS_PER_DAY: parameters[Constants.PUMP_RUNNING_HOURS_PER_DAY]
            }
            asset_list.append(asset)
    # Calculate daily running hours for each pump
    for asset in asset_list:
        try:
            logger.info('PRHD - Calculating Running Hours for %s', asset)
            calculated_object = get_raw_water_data_and_calculate_tag(resource, asset)
            water_utilities.save_calculated_water_tag(resource, calculated_object)
            logger.info('PRHD - Calculated Tag for Running hours pump : %s Saved', asset['assetId'])
        except WaterAnalyticsException as err:
            logger.error(err)


if __name__ == '__main__':
    calculate_running_hours_for_all_pumps('RWPH', 'Pump')