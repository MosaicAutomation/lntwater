from crontab import CronTab

water_cron = CronTab(user='ec2-user')
# RWPH
# Hourly
# pump_health_indicator
job0 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/rwph/pump_health_indicator.py')
job0.every(1).hours()

# Daily Once
# water_supply_per_day
job1 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/rwph/k01_water_supply_per_day.py')
job1.every().dom()

# p0xrhd_daily_pump_running_hours
job2 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/rwph/p0xrhd_daily_pump_running_hours.py')
job2.every().dom()

# p0xpcd_power_consumption
job3 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/rwph/p0xpcd_power_consumption.py')
job3.every().dom()

# k02_daily_aggregate_pump_running_hours
job4 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/rwph/k02_daily_aggregate_pump_running_hours.py')
job4.every().dom()

# k04_water_availability
job5 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/rwph/k04_water_availability.py')
job5.every().dom()

# k05_energy_consumption_per_day
job6 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/rwph/k05_energy_consumption_per_day.py')
job6.every().dom()

# k08_energy_consumption
job7 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/rwph/k08_energy_consumption_plant .py')
job7.every().dom()

# Daily Incidents
job10 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/rwph/daily_alarm_incidents.py')
job10.every().dom()

# Monthly
# k03_water_volume_supplied_per_month
job8 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/rwph/k03_water_volume_supplied_per_month.py')
job8.every().month()

# k06_energy_consumption_monthly
job9 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/rwph/k06_energy_consumption_monthly.py')
job9.every().month()


# CWPH
# Daily
cwph_k01 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/cwph/k01_volume_of_water_received_per_day.py')
cwph_k01.every().dom()

# Daily
cwph_k02 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/cwph/K02_03_water_processed_per_day.py')
cwph_k02.every().dom()

# Daily
cwph_k04 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/cwph/k04_daily_aggregate_pump_running_hours.py')
cwph_k04.every().dom()

# Monthly
cwph_k05 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/cwph/k05_water_volume_received_per_month.py')
cwph_k05.every().month()

# Monthly
cwph_k06 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/cwph/k06_07_water_volume_processed_per_month.py')
cwph_k06.every().month()

# Daily
cwph_k08 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/cwph/k08_water_availability_source1.py')
cwph_k08.every().dom()

# Daily
cwph_k09 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/cwph/k09_water_availability_source2.py')
cwph_k09.every().dom()

# Daily
cwph_k10 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/cwph/k10_energy_consumption_per_day.py')
cwph_k10.every().dom()

# Monthly
cwph_k11 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/cwph/k11_energy_consumption_monthly.py')
cwph_k11.every().month()

# Daily
cwph_k13 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/cwph/k13_energy_consumption_cwph_plant.py')
cwph_k13.every().dom()

# Daily - Power Consumption of all pump
cwph_pc = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/cwph/p0xpcd_power_consumption.py')
cwph_pc.every().dom()

# Daily - CWPH Running Hours
cwph_prh = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/cwph/p0xrhd_daily_pump_running_hours.py')
cwph_prh.every().dom()

# Hourly - Pump Health Index Frequency is Hourly
cwph_phi = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/cwph/cwph_pump_health_indicator')
cwph_phi.every(1).hours()


# WTP
# Daily
wtp_k01 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k01_water_filtered_per_day.py')
wtp_k01.every().dom()

# Daily
wtp_k02 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k02_aggregate_filter_running_hours.py')
wtp_k02.every().dom()

# Monthly
wtp_k03 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k03_wtp_processed_volume_monthly.py')
wtp_k03.every().month()

#Daily
wtp_k04 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k04_water_used_in_backwash_operation.py')
wtp_k04.every().dom()

# Daily
wtp_k05 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k05_wtp_energy_consumption.py')
wtp_k05.every().dom()

# Monthly
wtp_k06 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k06_wtp_energy_consumption_monthly.py')
wtp_k06.every().month()

# Daily
wtp_k07 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k07_wtp_efficiency.py')
wtp_k07.every().dom()

# Daily
wtp_k11 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k11_volume_of_backwash_tank.py')
wtp_k11.every().dom()

# Daily
wtp_k12 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k12_volume_supplied_to_deoli.py')
wtp_k12.every().dom()

# Daily
wtp_k13 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k13_volume_supplied_tonk_per_day.py')
wtp_k13.every().dom()

# Daily
wtp_k14 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k14_volume_supplied_to_uniyara.py')
wtp_k14.every().dom()

# Daily
wtp_k15 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k15_average_turbidity_per_day.py')
wtp_k15.every().dom()

# Daily
wtp_k16 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k16_energy_unit_consumption.py')
wtp_k16.every().dom()

# Daily
wtp_k17 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k17_alum_unit_consumption.py')
wtp_k17.every().dom()

# Daily
wtp_k18 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k18_lime_unit_consumption.py')
wtp_k18.every().dom()

# Daily
wtp_k19 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k19_chlorine_consumption.py')
wtp_k19.every().dom()

# Monthly
wtp_k20 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k20_alum_monthly_consumption.py')
wtp_k20.every().month()

# Monthly
wtp_k21 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k21_lime_monthly_consumption.py')
wtp_k21.every().month()

# Monthly
wtp_k22 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/k22_chlorine_monthly_consumption.py')
wtp_k22.every().month()

# Every 15 minutes
wtp_fe = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/wtp_filter_efficiency.py')
wtp_fe.every(15).minutes()

# Every Hour
wtp_fsm = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/wtp_filter_status_modes.py')
wtp_fsm.every(1).hours()

# Daily
wtp_frhd = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/f0xrhd_wtp_running_hours_per_day.py')
wtp_frhd.every().dom()

# Daily
wtp_fq01 = water_cron.new(command='python36 /home/ec2-user/lnt-water-analytics/water_analytics/wtp/fq01_wtp_backwash_flow_totalizer.py')
wtp_fq01.every().dom()

water_cron.write()
