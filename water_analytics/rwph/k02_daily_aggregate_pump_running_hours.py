import sys
from datetime import datetime, timedelta
import logging
# sys.path.append("D:\MB\Final Version\lnt-water-analytics\\")
from dateutil.relativedelta import relativedelta
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.RWPH_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def plant_pumps_daily_running_hours(resource, plant_id, final_value, time_stamp):
    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    plant_data = plant_table.get_item(
            Key={'id': Region.R1, Constants.PLANT_ID_KEY: plant_id}
        )
    print('Plant Data : ', plant_data)
    plant_details = plant_data['Item']
    parameters = plant_details[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]

    tag_yesterday = time_stamp + " 00:00:00"
    calculated_document = dict()
    calculated_document[Constants.TAG_VALUE_KEY] = str(final_value)
    calculated_document[Constants.TAG_ID_KEY] = decimal.Decimal(
        parameters[Constants.NUMBER_OF_RUNNING_HOURS_PER_DAY][Constants.TAG_ID_KEY])
    calculated_document[Constants.TIME_STAMP] = tag_yesterday

    calculated_tag_obj = {
        Constants.TAG_ID_KEY: decimal.Decimal(
            parameters[Constants.NUMBER_OF_RUNNING_HOURS_PER_DAY][Constants.TAG_ID_KEY]),
        Constants.TIME_STAMP: tag_yesterday,
        Constants.DOCUMENT_KEY: calculated_document
    }
    return calculated_tag_obj


def calculate_aggregate_running_hours_of_all_pumps(plant_type, asset_type):
    """
    This function calculates the daily running hours for all pumps individually.
    i.e. running hours per day for each pump
    The KPI Calculation id K02 and Tag Id 1002002

    :param plant_type:
    :param asset_type:
    :return: None
    """
    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    resource = AWSService(Services.DYNAMO_DB)
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, plant_type, 'Pump House')
    logger.info('K02 - Getting data for plants : %s', plant_ids)
    # Search for a Source, For that WaterAsset table is used.
    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    for plant_key in plant_ids:
        try:

            source_assets = asset_table.query(
                KeyConditionExpression=Key('id').eq(plant_key),
                FilterExpression=Attr('document.parentId').contains(plant_type) & Attr('document.attributes.assetType').eq(asset_type)
            )
            print(source_assets)
            logger.info('K02 - %s source items found ', source_assets['Count'])
            final_value = 0
            for sa in source_assets['Items']:
                try:
                    # assetId, runningHours, pumpRunningHoursPerDay
                    parameters = sa[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
                    tag_id = decimal.Decimal(parameters[Constants.PUMP_RUNNING_HOURS_PER_DAY][Constants.TAG_ID_KEY])
                    data = water_utilities.get_calculated_opera_water_data(resource, tag_id, str_yesterday)
                    print('Data : ', data)
                    value = decimal.Decimal(data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY])
                    logger.info('K02 - Tag ID %s & Value %s ', data[Constants.TAG_ID_KEY], value)
                    final_value = final_value + value
                except WaterAnalyticsException as err:
                    logger.error('K02 - %s', err)
            print('Final Value - ', final_value)
            calculated_object = plant_pumps_daily_running_hours(resource, plant_key, final_value, str_yesterday)
            print('Calculated Object :- ', calculated_object)
            # water_utilities.save_calculated_water_tag(resource, calculated_object)
            logger.info('K02 - Calculated Object : %s', calculated_object)
        except WaterAnalyticsException as err:
            logger.error(err)
        except Exception as err:
            logger.error(err)


if __name__ == '__main__':
    calculate_aggregate_running_hours_of_all_pumps('RWPH', 'Pump')
    # calculate_aggregate_running_hours_of_all_pumps('CWPH', 'Pump')