import sys
from datetime import datetime, timedelta
import logging
from dateutil.relativedelta import relativedelta
# sys.path.append("D:\MB\Final Version\lnt-water-analytics\\")
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

logger_file = water_utilities.get_logger_file_name(Constants.CWPH_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_raw_water_data_and_calculate_tag(resource, kwargs):

    today_date = datetime.now().strftime("%Y-%m-%d")

    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    today_date = today_date + " 00:00:00"
    # Getting Raw Data from OperaWaterTag table
    water_tag_table = resource.get_table(Tables.OPERA_WATER_TAGS)
    # Getting Recent Data based on yesterday date
    logger.info('PPCD - KWARGS : %s', kwargs)
    power_consumption_key = decimal.Decimal(kwargs[Constants.POWER_CONSUMPTION_KEY][Constants.TAG_ID_KEY])
    print("Power Consumption Key : ", power_consumption_key)
    try:
        raw_data = water_tag_table.query(
            KeyConditionExpression=Key(Constants.TAG_ID_KEY).eq(power_consumption_key)
            & Key(Constants.TIME_STAMP).begins_with(today_date),
            ScanIndexForward=False,
            Limit=1
        )
        print('RAW DATA : ', raw_data)
        power_data = raw_data['Items'][0]
        logger.info('PPCD - Raw Power Data Found for power consumption id %s and date %s - %s', power_consumption_key, str_yesterday, power_data )
        tag_value = power_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
        print('Tag Value : ', tag_value)

        tag_yesterday = str_yesterday + " 00:00:00"

        calculated_document = dict()
        calculated_document[Constants.TAG_VALUE_KEY] = str(tag_value)
        calculated_document[Constants.TAG_ID_KEY] = decimal.Decimal(
            kwargs[Constants.PUMP_POWER_CONSUMPTION_PER_DAY_KEY][Constants.TAG_ID_KEY])
        calculated_document[Constants.TIME_STAMP] = tag_yesterday

        calculated_tag_obj = {
            Constants.TAG_ID_KEY: decimal.Decimal(
                kwargs[Constants.PUMP_POWER_CONSUMPTION_PER_DAY_KEY][Constants.TAG_ID_KEY]),
            Constants.TIME_STAMP: tag_yesterday,
            Constants.DOCUMENT_KEY: calculated_document
        }
        return calculated_tag_obj
    except IndexError as err:
        logger.error('PPCD - %s', err)
        raise WaterAnalyticsException("PPCD - Raw Power Data is Not Found for " + str(power_consumption_key))


def calculate_daily_power_pump_consumption(water_type, asset_type):
    """
    This calculate hourly power consumption for each pump
    Frequency is hourly
    KPI Calculation ID: P01PCD	P02PCD	P03PCD	P04PCD	P05PCD	P06PCD	P07PCD	P08PCD	P09PCD	P10PCD
    Tag Id            : 1002127	1002128	1002129	1002130	1002131	1002132	1002133	1002134	1002135	1002136

    :param water_type:
    :param asset_type:
    :return:
    """
    resource = AWSService(Services.DYNAMO_DB)
    plant_ids = water_utilities.get_plant_ids_of_region(resource, Region.R1, water_type, 'Pump House')
    logger.info('PPCD - Getting data for plants : %s', plant_ids)
    # Search for a Source, For that WaterAsset table is used.
    asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
    asset_list = []
    for plant_key in plant_ids:
        source_assets = asset_table.query(
            KeyConditionExpression=Key('id').eq(plant_key),
            FilterExpression=Attr('document.parentId').contains(water_type) & Attr('document.attributes.assetType').eq(asset_type)
        )
        print('For Plant : ', plant_key, '  Source Count: ', source_assets['Count'])
        logger.info('PPCD - %s source items found : ', source_assets['Count'])
        for sa in source_assets['Items']:
            # customerTag, assetId, powerConsumption, pumpPowerConsumptionPerDay
            print('ASSET ID : ', sa[Constants.ASSET_ID_KEY])
            parameters = sa[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
            asset = {
                Constants.ASSET_ID_KEY: sa[Constants.ASSET_ID_KEY],
                Constants.POWER_CONSUMPTION_KEY: parameters[Constants.POWER_CONSUMPTION_KEY],
                Constants.PUMP_POWER_CONSUMPTION_PER_DAY_KEY: parameters[Constants.PUMP_POWER_CONSUMPTION_PER_DAY_KEY]
            }
            asset_list.append(asset)

    for asset in asset_list:
        try:
            logger.info('PPCD - Calculating power Consumption for %s', asset)
            calculated_object = get_raw_water_data_and_calculate_tag(resource, asset)
            water_utilities.save_calculated_water_tag(resource, calculated_object)
            logger.info('PPCD - Calculated Tag for power consumption of pump : %s Saved', asset['assetId'])
        except WaterAnalyticsException as err:
            logger.error(err)


if __name__ == '__main__':
    print("Invoked My Starter")
    calculate_daily_power_pump_consumption('CWPH', 'Pump')
