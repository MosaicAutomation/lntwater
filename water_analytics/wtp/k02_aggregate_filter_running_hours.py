import sys
from os import path
# sys.path.append("D:\MB\Final Version\lnt-water-analytics\\")
sys.path.append("/home/ec2-user/lnt-water-analytics/")
from water_analytics.utilities import *

from datetime import datetime, timedelta
import decimal
import logging
from boto3.dynamodb.conditions import Key, Attr

logger_file = water_utilities.get_logger_file_name(Constants.WTP_PLANT)
logging.basicConfig(filename=logger_file, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger(__name__)


def get_filter_running_value(resource, tag_id, day):
    try:
        running_data = water_utilities.get_calculated_opera_water_data(resource, tag_id, day)
        return running_data[Constants.DOCUMENT_KEY][Constants.TAG_VALUE_KEY]
    except WaterAnalyticsException as err:
        logger.error('WTP OPERATIONAL HOURS - %s', err)
        return 0


def calculate_aggregate_filter_running_hours_per_day(plant_type, asset_type, source_asset_type):
    today_date = datetime.now().strftime("%Y-%m-%d")
    yesterday_date = datetime.now().date() - timedelta(1)
    str_yesterday = yesterday_date.strftime("%Y-%m-%d")

    resource = AWSService(Services.DYNAMO_DB)

    plant_table = resource.get_table(Tables.OPERA_WATER_PLANT)
    wtp_plants = plant_table.query(
        KeyConditionExpression=Key('id').eq(Region.R1),
        FilterExpression=Attr('document.plantId').contains(plant_type)
    )
    for plant in wtp_plants['Items']:
        plant_id = plant[Constants.PLANT_ID_KEY]
        parameters = plant[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
        agggregate_running_hrs = parameters[Constants.WTP_NUMBER_OF_OPERATIONAL_HOURS_PER_DAY][Constants.TAG_ID_KEY]
        asset_table = resource.get_table(Tables.OPERA_WATER_ASSET)
        wtp_filters = asset_table.query(
            KeyConditionExpression=Key('id').eq(plant_id),
            FilterExpression=Attr('document.attributes.assetType').contains(source_asset_type)
        )
        final_value = 0
        for wtp_fltr in wtp_filters['Items']:
            print(wtp_fltr['assetId'])
            prmtrs = wtp_fltr[Constants.DOCUMENT_KEY][Constants.PARAMETERS_KEY]
            running_hrs_tag_id = prmtrs[Constants.WTP_NUMBER_OF_RUNNING_HOURS_PER_DAY][Constants.TAG_ID_KEY]
            value = get_filter_running_value(resource, decimal.Decimal(running_hrs_tag_id), str_yesterday)
            logger.info("Running Hours for tag id %s is %s", running_hrs_tag_id, value)
            final_value += float(value)

        logger.info('final Value : %s', final_value)
        calculated_obj = water_utilities.create_calculated_object(
            agggregate_running_hrs, final_value, str_yesterday + " 00:00:00")
        logger.info('calculated obj : %s', calculated_obj)
        water_utilities.save_calculated_water_tag(resource, calculated_obj)


if __name__ == '__main__':
    calculate_aggregate_filter_running_hours_per_day('WTP', 'Treatment Plant', 'Filter')
